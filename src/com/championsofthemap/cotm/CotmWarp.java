package com.championsofthemap.cotm;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class CotmWarp {

	private String name;
	private Location l;
	private Boolean cyclable;

	public CotmWarp(String name, World w, int x, int y, int z, int yaw, int pitch, Boolean cyclable) {
		this.name = name;
		this.l = new Location(w, x, y, z, yaw, pitch);
		this.cyclable = cyclable;
	}

	public CotmWarp(String name, Location l, Boolean cyclable) {
		this.name = name;
		this.l = l;
		this.cyclable = cyclable;
	}
	
	/**
	 * Creates a new warp from a string, in the cotm_warps.txt format. Caller should catch NumberFormatException.
	 * 
	 * @param warpParams String containing tab-separated warp parameters from the cotm_warps.txt file
	 * @param w The World in which the warps are to be created
	 */
	public CotmWarp(String warpParams, World w) {
		String[] warpParamArray = warpParams.split("\t", 7);
		
		this.name = warpParamArray[0];

		int x = Integer.parseInt(warpParamArray[1]);
		int y = Integer.parseInt(warpParamArray[2]);
		int z = Integer.parseInt(warpParamArray[3]);
		int yaw = Integer.parseInt(warpParamArray[4]);
		int pitch = Integer.parseInt(warpParamArray[5]);
		this.l = new Location (w, x, y, z, yaw, pitch);
		
		this.cyclable = (Integer.parseInt(warpParamArray[6]) != 0); 

	}

	public String getName() {
		return name;
	}

	public Location getLocation() {
		return l;
	}
	
	public Boolean isCyclable() {
		return cyclable;
	}

	public void warp(Player p) {
		p.teleport(l);
		if(p.getGameMode() == GameMode.CREATIVE)
			p.setFlying(true);
		p.sendMessage(Cotm.OK_COLOR + "You have been warped to " + name + "!");
	}
	
	/**
	 * Format the warp for output to cotm_warps.txt
	 * 
	 * @return A string in the format name[TAB]x[TAB]y[TAB]z[TAB]yaw[TAB]pitch[TAB]cyclable
	 */
	public String toFormattedString() {
		// The name, the x-coord, the y-coord, the z-coord, the yaw, the pitch, cyclable
		
		String s = name;
		s += "\t";
		s += l.getBlockX();
		s += "\t";
		s += l.getBlockY();
		s += "\t";
		s += l.getBlockZ();
		s += "\t";
		s += (int)l.getYaw();
		s += "\t";
		s += (int)l.getPitch();
		s += "\t";
		s += cyclable? "1" : "0";
		
		return s;
	}

}
