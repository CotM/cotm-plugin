package com.championsofthemap.cotm;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class CotmPlayer {

	private String playerName;
	private PlayerType pt;
	private String suffix;
	private Cotm c;

	private int warpIndex, playerIndex, teamIndex, kills, deaths;
	private Location tpBackLocation;

	public CotmPlayer(String name, String suffix, PlayerType pt, Cotm c) {
		this.playerName = name;
		this.suffix = suffix;
		this.pt = pt;
		this.c = c;

		this.warpIndex = 0;
		this.teamIndex = 0;
		this.kills = 0;
		this.deaths = 0;
		this.tpBackLocation = null;
	}

	public boolean isStaff() {
		return pt.isStaff();
	}

	public boolean isPlayer() {
		return pt == PlayerType.PLAYER;
	}
	
	public PlayerType getPlayerType(){
		return pt;
	}

	public String getSuffix() {
		return suffix;
	}

	public String getName() {
		return playerName;
	}

	public void setStatus() {
		if (pt == PlayerType.PLAYER)
			c.player(Bukkit.getPlayer(playerName), c.getTeamBySuffix(suffix), true);
		else if (pt == PlayerType.REF)
			c.makeRef(Bukkit.getPlayer((playerName)));
		else if (pt == PlayerType.STREAMER)
			c.makeStreamer(Bukkit.getPlayer((playerName)));
		else if (pt == PlayerType.ASSISTANT)
			c.makeAssistant(Bukkit.getPlayer((playerName)));
	}

	/**
	 * This sets the
	 * 
	 * @param l the location where the player is before he is teleported.
	 */
	public void setTpBackLocation(Location l) {
		this.tpBackLocation = l;
	}

	/**
	 * This method is used to get the location where the player was before he
	 * was teleported/warped
	 * 
	 * @return the location where the player used to be.
	 */
	public Location getTpBackLocation() {
		return tpBackLocation;
	}

	/**
	 * Teleports this player to their tpbacklocation.
	 * 
	 * @return true if tp succeeded, false if no location was previously stored.
	 */
	public boolean tpBack() {
		if (tpBackLocation != null) {
			c.doTeleport(this.getPlayer(), tpBackLocation);
			return true;
		}
		return false;
	}

	/**
	 * Increases the kill count by one.
	 */
	public void addKill() {
		kills++;
	}

	/**
	 * Returns the number of kills by this player.
	 * 
	 * @return the number of kills by this player
	 */
	public int getKills() {
		return kills;
	}

	/**
	 * Increases the death count by one.
	 */
	public void addDeath() {
		deaths++;
	}

	/**
	 * Returns the number of deaths by this player.
	 * 
	 * @return the number of deaths of this player
	 */
	public int getDeaths() {
		return deaths;
	}

	/**
	 * Resets kills and deaths for this player
	 */
	public void resetStats() {
		kills = 0;
		deaths = 0;
	}
	
	/**
	 * Returns the index of the previous warp in the warp cycle
	 * 
	 * @param numberOfWarps this is the number of warps on the map
	 * @return the warpIndex of the previous warp
	 */
	public int prevWarpIndex(int numberOfWarps) {
		warpIndex--;
		if (warpIndex < 0)
			warpIndex = numberOfWarps - 1;
		return warpIndex;
	}

	/**
	 * Returns the index of the previous warp in the warp cycle
	 * 
	 * @param numberOfWarps this is the number of warps on the map
	 * @return the warpIndex of the next warp
	 */
	public int nextWarpIndex(int numberOfWarps) {
		warpIndex++;
		if (warpIndex == numberOfWarps)
			warpIndex = 0;
		return warpIndex;
	}

	/**
	 * Returns the index of the previous player in the player cycle
	 * 
	 * @param numberOfPlayers this is the number of players playing
	 * @return the playerIndex of the previous player
	 */
	public int prevPlayerIndex(int numberOfPlayers) {
		playerIndex--;
		if (playerIndex < 0)
			playerIndex = numberOfPlayers - 1;
		return playerIndex;
	}

	/**
	 * Returns the index of the next player in the player cycle
	 * 
	 * @param numberOfPlayers this is the number of players playing
	 * @return the playerIndex of the next player
	 */
	public int nextPlayerIndex(int numberOfPlayers) {
		playerIndex++;
		if (playerIndex == numberOfPlayers)
			playerIndex = 0;
		return playerIndex;
	}

	
	/**
	 * Returns the index of the current player in the player cycle
	 * 
	 * @return the playerIndex of the current player
	 */
	public int getPlayerIndex() {
		return playerIndex;
	}
	
	
	/**
	 * Move forward in the team cycle
	 * 
	 * @param numberOfTeams The number of teams playing
	 * @return the new teamIndex
	 */
	public int nextTeamIndex(int numberOfTeams) {
		teamIndex++;
		if (teamIndex > numberOfTeams)
			teamIndex = 0;
		return teamIndex;
	}
	
	/**
	 * Move backward in the team cycle
	 * 
	 * @param numberOfTeams The number of teams playing
	 * @return the new teamIndex
	 */
	public int prevTeamIndex(int numberOfTeams) {
		teamIndex--;
		if (teamIndex < 0)
			teamIndex = numberOfTeams;
		return teamIndex;
	}
	
	/**
	 * Returns the index of the current team in the team cycle
	 * 
	 * @return the teamIndex of the current team
	 */
	public int getTeamIndex() {
		return teamIndex;
	}
	
	/**
	 * Set index of the current team in the team cycle
	 * 
	 * @param teamIndex the teamIndex to set as current
	 */
	public void setTeamIndex(int teamIndex) {
		this.teamIndex=teamIndex;
	}
	
	/**
	 * Get the Player associated with the CotmPlayer
	 * 
	 * @return the player associated with this CotmPlayer
	 */
	public Player getPlayer() {
		return Bukkit.getPlayerExact(playerName);
	}
}
