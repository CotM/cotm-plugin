package com.championsofthemap.cotm;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Animals;
import org.bukkit.entity.EnderDragon;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.Golem;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Item;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.MagmaCube;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.entity.Slime;
import org.bukkit.entity.Squid;
import org.bukkit.entity.Villager;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.championsofthemap.cotm.loserdeath.LoserDeath;

/**
 * @author caske33
 * @author martin
 * 
 */
public class Cotm extends JavaPlugin {

	private int countdownover = 0, countdownoverSumo = 0;
	private Calendar start, end;
	private World world;
	private long time;
	private ButcherType matchButcherType;
	private Trophy trophy;
	private Team champions;
	private Boolean disqualification = false;
	private static final String SUFFIX_REF = "ref", SUFFIX_STREAMER = "rec", SUFFIX_ASSISTANT = "staff";
	private static final String MAP_DATA_FILE = "cotm_data.txt", MAP_DATA_FORMAT_STRING = "CotM MapData v1";
	public static final Material ITEM_IN_HAND_1 = Material.RECORD_3, ITEM_IN_HAND_2 = Material.RECORD_4, ITEM_IN_HAND_3 = Material.RECORD_10,
			FOOD_ITEM = Material.RECORD_5, HEALTH_ITEM = Material.RECORD_6;
	private static final int MINUTE_ALERTS = 15;
	public static final ChatColor MAIN_COLOR = ChatColor.GREEN, SIDE_COLOR = ChatColor.GOLD, OK_COLOR = ChatColor.GREEN, ERROR_COLOR = ChatColor.RED,
			DECISION_COLOR = ChatColor.GOLD, ALERT_COLOR = ChatColor.GREEN, COUNTDOWN_COLOR = ChatColor.GOLD, COUNTDOWN_SIDE_COLOR = ChatColor.WHITE;
	private ArrayList<CotmWarp> warps = new ArrayList<CotmWarp>();
	private ArrayList<CotmWarp> warpCycle = new ArrayList<CotmWarp>();
	private ArrayList<CotmPlayer> teamPlayers = new ArrayList<CotmPlayer>();
	private ArrayList<Team> teams = new ArrayList<Team>();
	/**
	 * The cotmPlayers hashmap is a map of the lowercase name and the CotmPlayer
	 * associated with it.
	 */
	private HashMap<String, CotmPlayer> cotmPlayers = new HashMap<String, CotmPlayer>();
	private String mapName;
	private CotmListener l;
	private File fWarps;
	private String worldFolder;
	private String statsHtml;
	private String uploadUrl;
	private GameType gameType = null;
	private String gameTitle = null;
	private int minuteAlertsScheduler, endReminderScheduler;
	private static final String COLOR_NAMES[] = { "BLACK", "DARK_BLUE", "DARK_GREEN", "DARK_AQUA", "DARK_RED", "DARK_PURPLE", "GOLD", "GRAY",
			"DARK_GRAY", "BLUE", "GREEN", "AQUA", "RED", "LIGHT_PURPLE", "YELLOW", "WHITE", "MAGIC" };
	private static final SpecialMob SPECIAL_MOBS[] = { SpecialMob.IMFLAMMABLE_VILLAGER, SpecialMob.INVINCIBLE_PIG, SpecialMob.JUMP_SPIDER,
			SpecialMob.POISONED_COW };
	private static final String COLOR_DATA[] = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "k" };
	private static final String STATISTICS_POST_URL = "http://pastehtml.com/upload/create?input_type=html&result=address";
	private static final String STATISTICS_STYLESHEET_URL = "http://assets.championsofthemap.com/css/rawstats.css";
	private static final String RFWBOT_API_URL = "http://rfwbot.championsofthemap.com:2500/announce";
	private static final String RFWBOT_API_KEY = "ab523f85-782a-4c62-83d8-df77758e4e14";
	private static final String RFWBOT_STATS_CHANNEL = "cotm-match-alerts";
	private static final String CHALLENGER_LIST_URL = "http://www.championsofthemap.com/champions-and-challengers";
	
	private Random r = new Random();
	private Location lastNotifierLocation;
	private Location lastDeathLocation;
	private String lastDeathPlayerName;
	private Location lastEventLocation;
	private Location lastLogoutLocation;
    public final static HashMap<String, String> WarningMessages = new HashMap<String, String>();

	public void onEnable() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		this.saveDefaultConfig();
		
		l = new CotmListener(this);
		this.getServer().getPluginManager().registerEvents(l, this);

		this.world = getServer().getWorlds().get(0);
		loadMapData();
		
		getServer().broadcast(OK_COLOR + getDescription().getFullName() + " is running", Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
 		loadWarningMessages();
		if (this.getDebugMode())
			getServer().broadcast(ALERT_COLOR + "The CotM plugin is running in DEBUG mode. Use /debug to change this.", Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
	}

	public void onDisable() {
		saveMapData();
		this.saveConfig();
	}

	public boolean onCommand(CommandSender commandSender, Command command, String commandLabel, String[] args) {
		if (commandSender instanceof Player) {
			Player sender = (Player) commandSender;
			String cmd = command.getName().toLowerCase();

			if (sender.isOp()) {
				runCommandAsOp(sender, cmd, args);
			} else {
				runCommandAsPlayer(sender, cmd, args);
			}
		}
		return true;
	}

	/**
	 * Runs a command as Op
	 * 
	 * @param sender the Player who sent this command
	 * @param command the command
	 * @param args arguments
	 */
	public void runCommandAsOp(Player sender, String command, String[] args) {
		String response = null; // Stores any response to be given to the sender

		if (command.equals("gg")) {
			response = cGg(sender, args);
		} else if (command.equals("dq")) {
			response = cDq(sender,args);
		} else if (command.equals("ready")) {
			if (readyForGame(sender))
				response = OK_COLOR + "Everything is ready for the game.";
		} else if (command.equals("teaminfo")) {
			response = cTeaminfo(args);
		} else if (command.equals("delteam") || command.equals("teamdel")) {
			response = cDelteam(args);
		} else if (command.equals("clearteams")) {
			response = cClearteams();
		} else if (command.equals("trophy")) {
			response = cTrophy();
		} else if (command.equals("trophytp")) {
			response = cTrophytp();
		} else if (command.equals("gm")) {
			sender.setGameMode((sender.getGameMode() == GameMode.SURVIVAL) ? GameMode.CREATIVE : GameMode.SURVIVAL);
		} else if (command.equals("specialspawn") || command.equals("ss")) {
			specialSpawn(sender.getLocation());
		} else if (command.equals("tp")) {
			response = cTp(sender, args);
		} else if (command.equals("tpd")) {
			response = cTpd(sender);
		} else if (command.equals("tpl")) {
			response = cTpl(sender);
		} else if (command.equals("tpn")) {
			response = cTpn(sender);
		} else if (command.equals("tphere")) {
			response = cTphere(sender, args);
		} else if (command.equals("tpback")) {
			response = cTpback(sender, args);
		} else if (command.equals("warp")) {
			response = cWarp(sender, args);
		} else if (command.equals("prevwarp")) {
			response = cPrevwarp(sender);
		} else if (command.equals("nextwarp")) {
			response = cNextwarp(sender);
		} else if (command.equals("prevplayer")) {
			response = cPrevplayer(sender);
		} else if (command.equals("nextplayer")) {
			response = cNextplayer(sender);
		} else if (command.equals("delwarp") || command.equals("warpdel")) {
			response = cDelwarp(args);
		} else if (command.equals("addwarp") || command.equals("warpadd")) {
			response = cAddwarp(sender, args);
		} else if (command.equals("tpall")) {
			response = cTpall(sender, args);
		} else if (command.equals("gamedetails") || command.equals("gd")) {
			if (readyForGame(sender)) {
				announceGameDetails(Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
				announceGameDuration(Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
				announceStatsUrl(Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
			}
		} else if (command.equals("match") || command.equals("scrim")) {
			if (command.equals("scrim"))
				setGameType(GameType.SCRIMMAGE);
			response = cMatch(sender, args);
		} else if (command.equals("sumo")) {
			response = cSumo(sender, args);
		} else if (command.equals("cancel")) {
			response = cCancel();
		} else if (command.equals("endmatch")) {
			response = cEndmatch();
		} else if (command.equals("heal")) {
			response = cHeal(args);
		} else if (command.equals("healall")) {
			response = cHealall();
		} else if (command.equals("feed")) {
			response = cFeed(args);
		} else if (command.equals("feedall")) {
			response = cFeedall();
		} else if (command.equals("clearinv")) {
			response = cClearinv(args);
		} else if (command.equals("clearinvall")) {
			response = cClearinvall();
		} else if (command.equals("renew")) {
			response = cRenew(args);
		} else if (command.equals("renewall")) {
			response = cRenewall();
		} else if (command.equals("butcher")) {
			response = cButcher(args);
		} else if (command.equals("cleardrops")) {
			response = cClearDrops();
		} else if (command.equals("isop")) {
			response = cIsop();
		} else if (command.equals("oponly")) {
			response = cOponly(args);
		} else if (command.equals("vanish")) {
			response = cVanish(sender, args);
		} else if (command.equals("unvanish")) {
			response = cUnvanish(sender, args);
		} else if (command.equals("ref")) {
			response = cRef(sender, args);
		} else if (command.equals("streamer")) {
			response = cStreamer(sender, args);
		} else if (command.equals("assistant")) {
			response = cAssistant(sender, args);
		} else if (command.equals("unstaff")) {
			response = cUnstaff(sender, args);
		} else if (command.equals("team")) {
			response = cTeam(args);
		} else if (command.equals("addteam") || command.equals("teamadd")) {
			response = cAddteam(args);
		} else if (command.equals("teamcolor") || command.equals("teamcolour")) {
			response = cTeamcolor(args);
		} else if (command.equals("teamname")) {
			response = cTeamname(args);
		} else if (command.equals("teampoints") || command.equals("teamvictories")) {
			response = cTeamvictories(args);
		} else if (command.equals("player")) {
			response = cPlayer(sender, args);
		} else if (command.equals("unplayer")) {
			response = cUnplayer(args);
		} else if (command.equals("loadteams")) {
			response = cLoadteams(sender, args);
		} else if (command.equals("decision") || command.equals("d")) {
			response = cDecision(sender, args);
		} else if (command.equals("map")) {
			response = cMap(args);
		} else if (command.equals("gametype")) {
			response = cGametype(args);
		} else if (command.equals("gametitle")) {
			response = cGametitle(args);
		} else if (command.equals("playerinventory") || command.equals("pi") || command.equals("vi")) {
			response = cPlayerinventory(sender, args);
		} else if (command.equals("kill") || command.equals("suicide")) {
			response = cKill(sender, args, command.equals("suicide"));
		} else if (command.equals("notify") || command.equals("n")) {
			response = cNotify(sender, args);
		} else if (command.equals("rules")) {
			response = cRules(sender);
		} else if (command.equals("warn")) {
			response = cWarn(sender, args);
		} else if (command.equals("servername")) {
			response = cServername(sender, args);
		} else if (command.equals("debug")) {
			response = cDebug(sender, args);
		} else if (command.equals("notifyadmin")) {
			response = cNotifyadmin(sender, args);
		} else if (command.equals("nextteam")) {
			response = cNextteam(sender);
		} else if (command.equals("prevteam")) {
			response = cPrevteam(sender);
		} else if (command.equals("follow")) {
			response = cFollow(sender, args);
		} else if (command.equals("unfollow")) {
			response = cUnfollow(sender);
		}

		if (response != null)
			sender.sendMessage(response);
	}



	/**
	 * Runs a command as a player
	 * 
	 * @param sender the Player who sent the command
	 * @param command the command
	 * @param args arguments
	 */
	public void runCommandAsPlayer(Player sender, String command, String[] args) {
		String response = null; // Stores any response to be given to the sender

		if (command.equals("kill")) {
			response = ERROR_COLOR + "You are not allowed to use the kill command!";
		} else if (command.equals("notify") || command.equals("n")) {
			response = cNotify(sender, args);
		}
		if (response != null) {
			sender.sendMessage(response);
		}
	}

	/**
	 * Carry out the /unfollow command
	 * 
	 * @param sender
	 * @param args
	 * @return
	 */
	private String cUnfollow(Player sender) {
		if (!isStaff(sender)) return ERROR_COLOR + "You are not a staff member!";
		
		setFollowedTeam(sender, 0);
		return OK_COLOR + "Now following all players";
	}

	/**
	 * Carry out the /follow command
	 * 
	 * @param sender
	 * @param args
	 * @return
	 */
	private String cFollow(Player sender, String[] args) {
		if (!isStaff(sender)) return ERROR_COLOR + "You are not a staff member!";
		
		if (args.length != 1) {
			int teamIndex = getCotmPlayer(sender).getTeamIndex();
			if (teamIndex > 0)
				return OK_COLOR + "Following " + teams.get(getCotmPlayer(sender).getTeamIndex()-1).getColoredTeamName();
			else
				return OK_COLOR + "Following all players";
		}
		
		Team toFollow = null;
		
		if (args[0].equals("1") || args[0].equals("2")) {
			int teamNumber = Integer.parseInt(args[0]);
			if (teamNumber <= this.teams.size())
				toFollow = this.teams.get(teamNumber-1);
			
		} else {
			toFollow = this.getTeamBySuffix(args[0]);
		}
		
		if (toFollow != null) {
			setFollowedTeam(sender,toFollow);
			return OK_COLOR + "Now following " + toFollow.getColoredTeamName();
		} else {
			return ERROR_COLOR + "Team " + SIDE_COLOR + args[0] + ERROR_COLOR + " not found";
		}
	}

	/**
	 * Carry out the /prevteam command
	 * 
	 * @param sender
	 * @return
	 */
	private String cPrevteam(Player sender) {
		if (!isStaff(sender)) return ERROR_COLOR + "You are not a staff member!";
		
		if (teams.size() == 0)
			return ERROR_COLOR + "There are no teams set";
		prevTeam(sender);
		return null;
	}

	/**
	 * Carry out the /nextteam command
	 * 
	 * @param sender
	 * @return
	 */
	private String cNextteam(Player sender) {
		if (!isStaff(sender)) return ERROR_COLOR + "You are not a staff member!";
		
		if (teams.size() == 0)
			return ERROR_COLOR + "There are no teams set";
		nextTeam(sender);
		return null;
	}
	

	/**
	 * Carry out the /notifyadmin command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cNotifyadmin(Player sender, String[] args) {
		if (!isStaff(sender)) return ERROR_COLOR + "You are not a staff member!";
		
		if (args.length == 0)
			return ERROR_COLOR + "A message must be given";
		
		String s = "";
		for (int i = 0; i < args.length; i++) {
			s += args[i] + " ";
		}
		s = s.substring(0, s.length() - 1);

		if (notifyAdminRfwbot(sender.getName() + ": " + s))
			return OK_COLOR + "Message was sent successfully";
		
		return ERROR_COLOR + "The message could not be sent. Please contact the admins another way.";
	}
	
	
	/**
	 * Carry out the /debug command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cDebug(Player sender, String[] args) {
		if (args.length == 0)
			// Display current debug status
			if (getDebugMode())
				return ALERT_COLOR + "The CotM plugin is running in DEBUG mode.";
			else
				return OK_COLOR + "The CotM plugin is NOT running in DEBUG mode.";
		
		if (args.length > 1)
			return ERROR_COLOR + "Too many parameters. Please enter \"/debug on\" or \"/debug off\"";
		
		if ("on".equals(args[0].toLowerCase()) || "1".equals(args[0])) {
			setDebugMode(true);
			return ALERT_COLOR + "The CotM plugin is now running in DEBUG mode.";
		}
		
		if ("off".equals(args[0].toLowerCase()) || "0".equals(args[0])) {
			setDebugMode(false);
			return ALERT_COLOR + "The CotM plugin is now NOT running in DEBUG mode.";
		}
		
		return ERROR_COLOR + "Input not understood. Please enter \"/debug on\" or \"/debug off\"";
	}
		
	
	/**
	 * Carry out the /servername command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cServername(Player sender, String[] args) {
		if (args.length == 0)
			// Display current server name
			return OK_COLOR + "This server is: " + this.getConfig().getString("settings.servername","unknown server");
		
		String s = "";
		for (int i = 0; i < args.length; i++) {
			s += args[i] + " ";
		}
		s = s.substring(0, s.length() - 1);
		this.getConfig().set("settings.servername", s);
		return OK_COLOR + "Server name has been set to: " + s;

	}

	
	/**
	 * Carry out the /warn command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cWarn(Player sender, String[] args) {
		if (!isStaff(sender))
			return ERROR_COLOR + "You are not a staff member.";

		String message = "";

		if (args.length == 0) {
			// Do list of warnings
			message += OK_COLOR + "The following warnings are available:\n";
			
			for (Map.Entry<String, String> entry : WarningMessages.entrySet()) {
				String key = entry.getKey();
			    String value = entry.getValue();
			    message += SIDE_COLOR + key + OK_COLOR + ": " + value + "\n";
			    
			}
			return message;
		}
		
		String warningType = args[0];
		
		if (args.length > 1) {
			// Prepend player names to the message
			for (int i = 1; i < args.length; i++) {
				if (i > 1) message += ", ";
				message += args[i];
			}
			message += ": ";
		}

		// Look up the warningType in the list, and put it into message
		String warningMessage = null;
		boolean ambiguous = false;
		
		for (String key : WarningMessages.keySet()) {
			if (key.toLowerCase().startsWith(warningType.toLowerCase())) {
				if (warningMessage == null)
					warningMessage = WarningMessages.get(key);
				else
					ambiguous=true;
			}
		}
		
		if (warningMessage == null)
			return ERROR_COLOR + "Specified warning message not found";
		message += warningMessage;
		
		if (!ambiguous) {
			getServer().broadcastMessage("<" + sender.getDisplayName() + "> " + DECISION_COLOR + message);
			return null;
		} else {
			return ERROR_COLOR + "More than one matching warning message was found for the string '" + warningType + "'.";
		}
	}
	
	
	private String cRules(Player sender) {
		if (!isRef(sender))
			return ERROR_COLOR + "You are not a referee.";

		String rulesMessage = WarningMessages.get("rules");
		if (rulesMessage == null)
			return ERROR_COLOR + "No rules message available to display.";
		
		getServer().broadcastMessage("<" + sender.getDisplayName() + "> " + DECISION_COLOR + rulesMessage);
		return null;
		
		
	}

	/**
	 * Carry out the /notify command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cNotify(Player sender, String[] args) {
		String s = "";
		if (args.length == 0)
			s = "no text";
		else {
			for (int i = 0; i < args.length; i++) {
				s += args[i] + " ";
			}
			s = s.substring(0, s.length() - 1);
		}

		getServer().broadcast(ALERT_COLOR + "[N]" + ChatColor.WHITE + " <" + sender.getDisplayName() + "> " + ALERT_COLOR + s,
				Server.BROADCAST_CHANNEL_ADMINISTRATIVE);

		setLastNotifierLocation(sender.getLocation());

		return sender.isOp() ? null : OK_COLOR + "The CotM staff have been notified!";
	}

	/**
	 * Carry out the /kill or /suicide command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @param suicide whether the /suicide variant was used
	 * @return response
	 */
	private String cKill(Player sender, String[] args, Boolean suicide) {
		if (args.length > 0) {
			Player player;

			player = getServer().getPlayer(args[0]);
			if (player == null)
				return ERROR_COLOR + "Player " + args[0] + " has not been found on the server.";

			if (suicide)
				clearInventory(player);

			player.setHealth(0);
			String s = "";
			if (args.length > 1) {
				for (int i = 1; i < args.length; i++) {
					s += " " + args[i];
				}
				getServer().broadcastMessage("<" + sender.getDisplayName() + "> " + DECISION_COLOR + s);
			} else {
				getServer().broadcastMessage(
						"<" + sender.getDisplayName() + "> " + DECISION_COLOR + player.getName() + " has been killed on my command.");
			}
			return null;
		}

		if (suicide)
			clearInventory(sender);

		sender.setHealth(0);
		return null;
	}

	/**
	 * Carry out the /playerinventory or /pi command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cPlayerinventory(Player sender, String[] args) {
		if (args.length < 1)
			return ERROR_COLOR + "Please give the name of a player";
		Player p = getServer().getPlayer(args[0]);
		if (p == null)
			return ERROR_COLOR + "Player " + args[0] + " has not been found on the server.";

		openInventoryPlayer(sender, p);
		return null;

	}

	/**
	 * Carry out the /gametitle command
	 * 
	 * @param args arguments
	 * @return response
	 */
	private String cGametitle(String[] args) {
		if (args.length == 0)
			return ERROR_COLOR + "Please specify the game title, or type " + SIDE_COLOR + "/gametitle default" + ERROR_COLOR + " to set the default";

		if (args[0].equalsIgnoreCase("default")) {
			setGameTitle(null);

		} else {

			String s = "";
			for (int i = 0; i < args.length; i++) {
				s += args[i] + " ";
			}
			setGameTitle(s.substring(0, s.length() - 1));
		}
		return OK_COLOR + "The game title has been set to " + SIDE_COLOR + getGameTitle();
	}

	/**
	 * Carry out the /gametype command
	 * 
	 * @param args arguments
	 * @return response
	 */
	private String cGametype(String[] args) {
		if (args.length == 0)
			return ERROR_COLOR
					+ "You haven't specified what the game type is. QF (Queue Fight), CM (Championship Match), MTM (Meet the Map) or SCRIM(Scrimmage).";

		if (args[0].equalsIgnoreCase("QF")) {
			setGameType(GameType.QUEUE_FIGHT);
		} else if (args[0].equalsIgnoreCase("CM")) {
			setGameType(GameType.CHAMPIONSHIP_MATCH);
		} else if (args[0].equalsIgnoreCase("MTM")) {
			setGameType(GameType.MEET_THE_MAP);
		} else if (args[0].equalsIgnoreCase("SCRIM")) {
			setGameType(GameType.SCRIMMAGE);
		} else {
			return ERROR_COLOR
					+ "The only available gametypes are QF (Queue Fight), CM (Championship Match), MTM (Meet the Map) and SCRIM(Scrimmage).";
		}

		return MAIN_COLOR + "The gametype of this match has been set to " + SIDE_COLOR + gameType.getName();
	}

	/**
	 * Carry out the /map command
	 * 
	 * @param args arguments
	 * @return response
	 */
	private String cMap(String[] args) {
		if (args.length < 1)
			return ERROR_COLOR + "You haven't specified a map name!";

		String s = "";
		for (int i = 0; i < args.length; i++) {
			s += args[i] + " ";
		}
		setMapName(s.substring(0, s.length() - 1));
		return MAIN_COLOR + "The map name has been set to " + SIDE_COLOR + mapName + "!";
	}

	/**
	 * Carry out the /decision or /dc command
	 * 
	 * @param sender sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cDecision(Player sender, String[] args) {
		if (!isRef(sender))
			return ERROR_COLOR + "You are not a referee.";
		
		if (args.length < 1) return ERROR_COLOR + "No decision specified";
		
		String s = "";
		for (int i = 0; i < args.length; i++) {
			s += args[i] + " ";
		}
		getServer().broadcastMessage("<" + sender.getDisplayName() + "> " + DECISION_COLOR + s.substring(0, s.length() - 1));
		return null;
	}

	/**
	 * Carry out the /player command
	 * 
	 * @param sender the player who sent the command
	 * @param args arguments
	 * @return response
	 */
	private String cPlayer(Player sender, String[] args) {
		if (args.length <= 1)
			return ERROR_COLOR + "You need to pass at least 2 parameters: /player [suffix] [name1]";

		if (args[0].length() > 5)
			return ERROR_COLOR + "The suffix can't be longer than 5 chars.";

		if (getTeamBySuffix(args[0]) == null)
			return ERROR_COLOR + "There is no team named " + args[0] + ". Create it with /addteam or /team";

		Team t = getTeamBySuffix(args[0]);
		String response = "";

		for (int i = 1; i < args.length; i++) {
			if (getServer().getPlayer(args[i]) != null) {
				Player p = getServer().getPlayer(args[i]);
				addCotmPlayer(new CotmPlayer(p.getName(), t.getSuffix(), PlayerType.PLAYER, this));
				Boolean deOp = true;
				if (p.getName().equals(sender.getName())) {
					deOp = false;
				}
				player(p, t, deOp);
				response += SIDE_COLOR + p.getName() + MAIN_COLOR + " is now a member of " + t.getColor() + t.getSuffix() + "\n";
			} else {
				addCotmPlayer(new CotmPlayer(args[i], t.getSuffix(), PlayerType.PLAYER, this));
				response += SIDE_COLOR + args[i] + MAIN_COLOR + " is now a member of " + t.getColor() + t.getSuffix() + "\n";
			}
		}
		return response;
	}

	private String cLoadteams(Player sender, String[] args) {
		if (args.length == 0)
			return ERROR_COLOR + "Please pass a URL to load teams from";

		YamlConfiguration cfg;
		try {
			cfg = getMatchConfig(args[0]);
		} catch (IOException e) {
			return ERROR_COLOR + "Bad URL or communication error. Unable to load data.";
		} catch (InvalidConfigurationException e) {
			return ERROR_COLOR + "The configuration data could not be understood. Did you specify the correct URL?";
		}

		String output = "";
		if (cfg != null) {
			teams.clear();
			clearTeamPlayers();
			
			// Add the first team
			String suffix = cfg.getString("team1.suffix");
			String name = cfg.getString("team1.name");
			String color = cfg.getString("team1.color");
			int victories = cfg.getInt("team1.victories");
			List<String> players = cfg.getStringList("team1.players");

			output += setTeam(sender, suffix, name, color, victories, players);

			// add the 2nd team
			suffix = cfg.getString("team2.suffix");
			name = cfg.getString("team2.name");
			color = cfg.getString("team2.color");
			victories = cfg.getInt("team2.victories");
			players = cfg.getStringList("team2.players");

			output += setTeam(sender, suffix, name, color, victories, players);

			// set the gametype
			String gametype = cfg.getString("gametype");
			if (gametype != null) {
				String args1[] = { "" };
				args1[0] = gametype;
				output += cGametype(args1) + "\n";
			}

			// set the gametype
			String gametitle = cfg.getString("gametitle");
			if (gametitle != null) {
				String args1[] = { "" };
				args1[0] = gametitle;
				output += cGametitle(args1) + "\n";
			}

		}

		return output;

	}

	/**
	 * Carry out the /unplayer command
	 * 
	 * @param sender the player who sent the command
	 * @param args arguments
	 * @return response
	 */
	private String cUnplayer(String[] args) {
		if (args.length == 0)
			return ERROR_COLOR + "You need to pass at least 1 parameter: /unplayer [name]";

		String response = "";

		for (int i = 0; i < args.length; i++) {
			CotmPlayer cp = getCotmPlayer(args[i]);
			if (cp != null) {
				if (cp.isPlayer()) {
					removeFromTeams(cp.getName());
					response += SIDE_COLOR + cp.getName() + MAIN_COLOR + " is no longer a team member." + "\n";
				} else
					response += ERROR_COLOR + "Player " + args[i] + " is not a player. Use /unstaff to unstaff a staff member." + "\n";
			} else {
				response += ERROR_COLOR + "Player " + args[i] + " is not registered." + "\n";
			}
		}
		return response;
	}

	/**
	 * Carry out the /teamvictories or /teampoints command
	 * 
	 * @param args arguments
	 * @return response
	 */
	private String cTeamvictories(String[] args) {
		if (args.length != 2)
			return ERROR_COLOR + "Usage: /teamvictories suffix victories | /teampoints suffix points";

		int victories = 0;
		try {
			victories = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {
		}
		Team team = getTeamBySuffix(args[0]);
		if (team == null)
			return ERROR_COLOR + "Team " + args[0] + " not found";

		team.setVictories(victories);
		return OK_COLOR + "Team " + team + OK_COLOR + " now has " + victories + " victories/points";
	}

	/**
	 * Carry out the /teamname command
	 * 
	 * @param args arguments
	 * @return response
	 */
	private String cTeamname(String[] args) {
		if (args.length < 2)
			return ERROR_COLOR + "Usage: /teamname suffix name";

		int i = 1;
		String name = args[i++];
		while (args.length > i) {
			name += " " + args[i++];
		}

		Team team = getTeamBySuffix(args[0]);
		if (team == null)
			return ERROR_COLOR + "Team " + args[0] + " not found";

		team.setTeamName(name);
		return OK_COLOR + "Team name updated: " + team;

	}

	/**
	 * Carry out the /teamcolor command
	 * 
	 * @param args arguments
	 * @return response
	 */
	private String cTeamcolor(String[] args) {
		if (args.length != 2)
			return ERROR_COLOR + "Usage: /teamcolor suffix color";

		int colorIndex = -1;
		String color = args[1];

		for (int i = 0; i < COLOR_NAMES.length; i++) {
			if (COLOR_NAMES[i].equalsIgnoreCase(color)) {
				colorIndex = i;
			}
		}
		if (colorIndex == -1)
			return ERROR_COLOR + "You didn't specify a valid color: " + StringUtils.join(COLOR_NAMES, ", ");

		Team team = getTeamBySuffix(args[0]);
		if (team == null)
			return ERROR_COLOR + "Team " + args[0] + " not found";

		ChatColor chatColor = ChatColor.getByChar(COLOR_DATA[colorIndex]);
		team.setColor(chatColor, getServer());
		return OK_COLOR + "Team color updated: " + team;
	}

	/**
	 * Carry out the /addteam command
	 * 
	 * @param args arguments
	 * @return response
	 */
	private String cAddteam(String[] args) {
		// Check how many teams there are yet
		if (teams.size() >= 2)
			return ERROR_COLOR + "Two teams exist already. To add more you must use /team. To reset teams, type /clearteams";

		ChatColor chatColor = ChatColor.RED;
		String suffix = "red";
		String name = "Red";

		if (teams.size() == 1) {
			// there is already a team; if that team is RED then the new
			// team will be BLUE
			if (teams.get(0).getColor() == ChatColor.RED)
				chatColor = ChatColor.BLUE;
			suffix = "blue";
			name = "Blue";
		}

		if (args.length > 0) {
			suffix = args[0];
			name = suffix;
			if (args.length > 1) {
				// get the name
				int i = 1;
				name = args[i++];
				while (args.length > i) {
					name += " " + args[i++];
				}
			}
		}

		if (suffix.length() > 5)
			return ERROR_COLOR + "The suffix can only be 5 chars long.";

		teams.add(new Team(-1, suffix, chatColor, name, this));
		return OK_COLOR + "Team created: " + chatColor + name + "(" + suffix + ")";

	}

	/**
	 * Carry out the /team command
	 * 
	 * @param args arguments
	 * @return response
	 */
	private String cTeam(String[] args) {
		if (args.length < 4)
			return ERROR_COLOR + "You need to pass the suffix, the number of victories and the team name: /team [victories] [suffix] [color] [name]";

		// get the suffix
		String suffix = args[1];
		if (suffix.length() > 5)
			return ERROR_COLOR + "The suffix can only be 5 chars long.";

		String color = args[2];
		int colorIndex = getColorIndex(color);

		if (colorIndex == -1)
			return ERROR_COLOR + "You didn't specify a valid color: " + StringUtils.join(COLOR_NAMES, ", ");

		// get the name
		int i = 3;
		String name = args[i++];
		while (args.length > i) {
			name += " " + args[i++];
		}
		// get the number of victories
		int victories = 0;
		try {
			victories = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
		}
		ChatColor chatColor = ChatColor.getByChar(COLOR_DATA[colorIndex]);
		if (teamExists(args[1])) {
			Team t = getTeamBySuffix(suffix);
			t.resetValues(victories, suffix, chatColor, name);
			return OK_COLOR + "Team has been reset: " + chatColor + name + "(" + suffix + ")" + OK_COLOR + " has " + chatColor + victories + OK_COLOR
					+ " victories.";
		} else {
			teams.add(new Team(victories, suffix, chatColor, name, this));
			return OK_COLOR + "Team has been set: " + chatColor + name + "(" + suffix + ")" + OK_COLOR + " has " + chatColor + victories + OK_COLOR
					+ " victories.";
		}
	}

	/**
	 * Carry out the /unstaff command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cUnstaff(Player sender, String[] args) {
		if (args.length == 0) {
			removeCotmPlayer(sender.getName());
			unstaff(sender, false);
			return null;
		}

		Player player;
		String response = "";
		for (int i = 0; i < args.length; i++) {
			player = getServer().getPlayer(args[i]);
			if (player == null) {
				response += ERROR_COLOR + "Player " + args[i] + " is not on the server!" + "\n";
			} else {
				removeCotmPlayer(player.getName());
				if (player.getName().equals(sender.getName()))
					unstaff(player, false);
				else
					unstaff(player, true);
				response += SIDE_COLOR + player.getName() + MAIN_COLOR + " is not a staff member anymore!" + "\n";
			}
		}
		return response;
	}

	/**
	 * Carry out the /assistant command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cAssistant(Player sender, String[] args) {
		if (args.length == 0) {
			addCotmPlayer(new CotmPlayer(sender.getName(), SUFFIX_ASSISTANT, PlayerType.ASSISTANT, this));
			makeAssistant(sender);
			return null;
		}
		Player player;
		String response = "";
		for (int i = 0; i < args.length; i++) {
			player = getServer().getPlayer(args[i]);
			if (player == null) {
				addCotmPlayer(new CotmPlayer(args[i], SUFFIX_ASSISTANT, PlayerType.ASSISTANT, this));
				response += SIDE_COLOR + args[i] + MAIN_COLOR + " is now an assistant referee." + "\n";
			} else {
				addCotmPlayer(new CotmPlayer(player.getName(), SUFFIX_ASSISTANT, PlayerType.ASSISTANT, this));
				makeAssistant(player);
				response += SIDE_COLOR + player.getName() + MAIN_COLOR + " is now an assistant referee." + "\n";
			}
		}
		return response;
	}

	/**
	 * Carry out the /streamer command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cStreamer(Player sender, String[] args) {
		if (args.length == 0) {
			addCotmPlayer(new CotmPlayer(sender.getName(), SUFFIX_STREAMER, PlayerType.STREAMER, this));
			makeStreamer(sender);
			return null;
		}

		Player player;
		String response = "";
		for (int i = 0; i < args.length; i++) {
			player = getServer().getPlayer(args[i]);
			if (player == null) {
				addCotmPlayer(new CotmPlayer(args[i], SUFFIX_STREAMER, PlayerType.STREAMER, this));
				response += SIDE_COLOR + args[i] + MAIN_COLOR + " is now a streamer." + "\n";
			} else {
				addCotmPlayer(new CotmPlayer(player.getName(), SUFFIX_STREAMER, PlayerType.STREAMER, this));
				makeStreamer(player);
				response += SIDE_COLOR + player.getName() + MAIN_COLOR + " is now a streamer." + "\n";
			}
		}
		return response;
	}

	/**
	 * Carry out the /vanish command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cVanish(Player sender, String[] args) {
		if (args.length == 0) {
			setVanish(sender);
			return null;
		}

		Player player;
		String response = "";
		for (int i = 0; i < args.length; i++) {
			player = getServer().getPlayer(args[i]);
			if (player == null) {
				response += ERROR_COLOR + "Player " + args[i] + " was not found on the server." + "\n";
			} else {
				setVanish(player);
				response += SIDE_COLOR + player.getName() + MAIN_COLOR + " is now vanished." + "\n";
			}
		}
		return response;
	}

	/**
	 * Carry out the /unvanish command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cUnvanish(Player sender, String[] args) {
		if (args.length == 0) {
			unvanish(sender);
			return null;
		}

		Player player;
		String response = "";
		for (int i = 0; i < args.length; i++) {
			player = getServer().getPlayer(args[i]);
			if (player == null) {
				response += ERROR_COLOR + "Player " + args[i] + " was not found on the server." + "\n";
			} else {
				unvanish(player);
				response += SIDE_COLOR + player.getName() + MAIN_COLOR + " is now unvanished." + "\n";
			}
		}
		return response;
	}

	/**
	 * Carry out the /ref command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cRef(Player sender, String[] args) {
		String response = "";

		if (args.length == 0) {
			addCotmPlayer(new CotmPlayer(sender.getName(), SUFFIX_REF, PlayerType.REF, this));
			makeRef(sender);
		} else {
			Player player;

			for (int i = 0; i < args.length; i++) {
				player = getServer().getPlayer(args[i]);
				if (player == null) {
					addCotmPlayer(new CotmPlayer(args[i], SUFFIX_REF, PlayerType.REF, this));
					response += SIDE_COLOR + args[i] + MAIN_COLOR + " is now a referee." + "\n";
				} else {
					addCotmPlayer(new CotmPlayer(player.getName(), SUFFIX_REF, PlayerType.REF, this));
					makeRef(player);
					response += SIDE_COLOR + player.getName() + MAIN_COLOR + " is now a referee." + "\n";
				}
			}
		}

		if (numberOfRefsOnline() > 1)
			response += ERROR_COLOR + "Warning: more than one referee. Use /assistant for other staff.";

		return response;
	}

	/**
	 * Carry out the /oponly command
	 * 
	 * @param args arguments
	 * @return response
	 */
	private String cOponly(String[] args) {
		ArrayList<Player> ops = new ArrayList<Player>();
		for (int i = 0; i < args.length; i++) {
			Player p = getServer().getPlayer(args[i]);
			setOp(p, true);
			ops.add(p);
		}
		for (Player p : world.getPlayers()) {
			if (!ops.contains(p))
				setOp(p, false);
		}
		return null;
	}

	/**
	 * Carry out the /isop command
	 * 
	 * @return response
	 */
	private String cIsop() {
		String s = OK_COLOR + "The current ops are: ";
		for (Player p : world.getPlayers()) {
			if (p.isOp()) {
				s += p.getName() + ", ";
			}
		}
		return s.substring(0, s.length() - 2);
	}

	/**
	 * Carry out the /butcher command
	 * 
	 * @param args arguments
	 * @return response
	 */
	private String cButcher(String[] args) {
		if (args.length == 0) {
			butcherAll();
			return null;
		}

		String response = "";
		for (int i = 0; i < args.length; i++) {
			ButcherType b = ButcherType.selectButcherType(args[i]);
			if (b == null)
				response += ERROR_COLOR + "The butcher option \"" + args[i] + "\" was not understood.\n";
			else
				butcher(b);
		}
		if (!response.isEmpty())
			return response + "Valid options are: hostile friendly all";
		else
			return null;
	}

	/**
	 * Carry out the /cleardrops command
	 * 
	 * @return response
	 */
	private String cClearDrops() {
		clearDrops();
		return null;
	}

	/**
	 * Carry out the /renewall command
	 * 
	 * @return response
	 */
	private String cRenewall() {
		renewAll();
		return OK_COLOR + "Renewed all players.";
	}

	/**
	 * Carry out the /renew command
	 * 
	 * @param args arguments
	 * @return response
	 */
	private String cRenew(String[] args) {
		if (args.length == 0)
			return ERROR_COLOR + "Please specify player(s) to heal, or use /healall";

		String response = "";

		for (int i = 0; i < args.length; i++) {
			Player p = getServer().getPlayer(args[i]);
			if (p == null) {
				response += ERROR_COLOR + "Player " + args[i] + " has not been found on the server." + "\n";
			} else {
				renew(p);
				p.sendMessage(OK_COLOR + "You have been healed and fed!");
				response += OK_COLOR + "Renewed " + p.getName() + "\n";
			}
		}

		return response;
	}

	/**
	 * Carry out the /clearinvall command
	 * 
	 * @return response
	 */
	private String cClearinvall() {
		for (Player p : world.getPlayers()) {
			if (p.getGameMode() == GameMode.SURVIVAL) {
				clearInventory(p);
				p.sendMessage(OK_COLOR + "Your inventory has been cleared");
			}
		}
		return OK_COLOR + "Cleared all survival players' inventories.";
	}

	/**
	 * Carry out the /clearinv command
	 * 
	 * @param args arguments
	 * @return response
	 */
	private String cClearinv(String[] args) {
		if (args.length == 0)
			return ERROR_COLOR + "Please specify player(s) to clear, or use /clearinvall";

		String response = "";
		for (int i = 0; i < args.length; i++) {
			Player p = getServer().getPlayer(args[i]);
			if (p == null) {
				response += ERROR_COLOR + "Player " + args[i] + " has not been found on the server." + "\n";
			} else {
				clearInventory(p);
				p.sendMessage(OK_COLOR + "Your inventory has been cleared");
				response += OK_COLOR + "Cleared inventory of " + p.getName() + "\n";
			}
		}

		return response;
	}

	/**
	 * Carry out the /feedall command
	 * 
	 * @return response
	 */
	private String cFeedall() {
		for (Player p : world.getPlayers()) {
			feed(p);
			p.sendMessage(OK_COLOR + "You have been fed");
		}
		return OK_COLOR + "Fed all players.";
	}

	/**
	 * Carry out the /feed command
	 * 
	 * @param args arguments
	 * @return response
	 */
	private String cFeed(String[] args) {
		if (args.length == 0)
			return ERROR_COLOR + "Please specify player(s) to feed, or use /feedall";

		String response = "";
		for (int i = 0; i < args.length; i++) {
			Player p = getServer().getPlayer(args[i]);
			if (p == null) {
				response += ERROR_COLOR + "Player " + args[i] + " has not been found on the server." + "\n";
			} else {
				feed(p);
				p.sendMessage(OK_COLOR + "You have been fed");
				response += OK_COLOR + "Restored food levels of " + p.getName() + "\n";
			}
		}

		return response;
	}

	/**
	 * Carry out the /healall command
	 * 
	 * @return response
	 */
	private String cHealall() {
		for (Player p : world.getPlayers()) {
			heal(p);
			p.sendMessage(OK_COLOR + "You have been healed");
		}
		return OK_COLOR + "Healed all players.";
	}

	/**
	 * Carry out the /heal command
	 * 
	 * @param args arguments
	 * @return response
	 */
	private String cHeal(String[] args) {
		if (args.length == 0)
			return ERROR_COLOR + "Please specify player(s) to heal, or use /healall";

		String response = "";
		for (int i = 0; i < args.length; i++) {
			Player p = getServer().getPlayer(args[i]);
			if (p == null)
				response += ERROR_COLOR + "Player " + args[i] + " has not been found on the server." + "\n";

			heal(p);
			p.sendMessage(OK_COLOR + "You have been healed");
			response += OK_COLOR + "Restored health of " + p.getName() + "\n";
		}

		return response;
	}

	/**
	 * Carry out the /cancel command
	 * 
	 * @return response
	 */
	private String cCancel() {
		if (countdownover > 0 || countdownoverSumo > 0) {
			countdownover = -1;
			countdownoverSumo = -1;
			return OK_COLOR + "Countdown has been cancelled!";
		}

		if (matchRunning())
			return ERROR_COLOR + "The match has already begun. Please use /endmatch to abort it";

		return ERROR_COLOR + "Countdown has not started!";
	}

	private String cEndmatch() {
		if (!matchRunning())
			return ERROR_COLOR + "No match is currently in progress!";

		start = null;
		cancelMinuteAlerts();
		return OK_COLOR + "The current match has been aborted.";

	}

	/**
	 * Carry out the /sumo command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cSumo(Player sender, String[] args) {
		if (!isRef(sender))
			return ERROR_COLOR + "You're not a referee!";

		if (countdownover > 0)
			return ERROR_COLOR + "Countdown has already begun!";

		countdownoverSumo = 5;
		if (args.length >= 1) {
			try {
				countdownoverSumo = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
			}
		}
		if (countdownoverSumo < 5) {
			renewAll();
		} else if (countdownoverSumo > 5) {
			getServer().broadcastMessage(MAIN_COLOR + "Hunger & Health will be done 5 seconds before GO!");
		}
		countdownSumo();
		return null;
	}

	/**
	 * Carry out the /match command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cMatch(Player sender, String[] args) {
		if (!isRef(sender) && gameType != GameType.SCRIMMAGE)
			return ERROR_COLOR + "You're not a referee!";

		if (countdownover > 0)
			return ERROR_COLOR + "Countdown has already begun!";

		if (matchRunning())
			return ERROR_COLOR + "The match has already begun. Please use /endmatch to abort it";
		
		
		if (!readyForGame(sender))
			return null;

		Long gameStartTime = 0L;
		Integer countdownLength = 15;
		if (args.length >= 1) {
			if (args[0].equalsIgnoreCase("dusk")) {
				gameStartTime = 20L * 60 * 10;
			} else if (args[0].equalsIgnoreCase("night")) {
				gameStartTime = 12540L;
			} else if (args[0].equalsIgnoreCase("dawn")) {
				gameStartTime = 20L * 60 * (37 / 2);
			} else if (args[0].equalsIgnoreCase("day")) {
				gameStartTime = 0L;
			} else {
				try {
					gameStartTime = Long.parseLong(args[0]);
				} catch (NumberFormatException e) {
					return ERROR_COLOR + "Time '" + args[0] + "' was not understood.";
				}
			}

		}
		if (args.length >= 2) {
			try {
				countdownLength = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
				return ERROR_COLOR + "Countdown length '" + args[1] + "' was not understood.";
			}
		}

		matchButcherType = ButcherType.BUTCHER_ALL;
		if (args.length >= 3) {
			ButcherType b = ButcherType.selectButcherType(args[2]);
			if (b == null)
				return ERROR_COLOR + "The butcher type " + args[2] + " was not understood.";
			matchButcherType = b;
		}
		startCountdown(gameStartTime, countdownLength);

		return null;

	}

	/**
	 * Carry out the /tpall command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cTpall(Player sender, String[] args) {
		if (args.length == 0) {
			for (Player p : world.getPlayers()) {
				if (p.getGameMode() != GameMode.CREATIVE) {
					p.teleport(sender);
					p.sendMessage(OK_COLOR + "You have been teleported!");
				}
			}
			return OK_COLOR + "Players have been teleported to you!";
		}

		if (getTeamBySuffix(args[0]) == null)
			return ERROR_COLOR + "There is no team with suffix " + args[0] + "!";

		Team t = getTeamBySuffix(args[0]);
		for (Player p : t.getTeamPlayers()) {
			p.teleport(sender);
			p.sendMessage(OK_COLOR + "You have been teleported!");
		}

		return OK_COLOR + "Players have been teleported to you!";
	}

	/**
	 * Carry out the /addwarp command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cAddwarp(Player sender, String[] args) {
		if (args.length == 0)
			return ERROR_COLOR + "You didn't specify the warpname: /addwarp [name]";

		CotmWarp oldwarp = getWarpByName(args[0]);
		if (oldwarp != null) {
			warps.remove(oldwarp);
		}

		Boolean cyclable = true;
		if (args.length >= 2) {
			if (args[1].equals("0"))
				cyclable = false;
		}
		CotmWarp w = new CotmWarp(args[0], sender.getLocation(), cyclable);
		warps.add(w);
		if (cyclable)
			warpCycle.add(w);

		return OK_COLOR + "The warp " + args[0] + " was created at your location.";

	}

	/**
	 * Carry out the /delwarp command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cDelwarp(String[] args) {
		if (args.length == 0)
			return ERROR_COLOR + "You didn't specify the warpname: /delwarp [name]" + "\n" + ERROR_COLOR + "Existing warps: " + getAllWarpNames();

		CotmWarp warp = getWarpByName(args[0]);
		if (warp == null)
			return ERROR_COLOR + "Warp " + args[0] + " doesn't exist!" + "\n" + ERROR_COLOR + "Existing warps: " + getAllWarpNames();

		warps.remove(warp);
		warpCycle.remove(warp);
		return OK_COLOR + "Warp " + args[0] + " is deleted!";
	}

	/**
	 * Carry out the /nextplayer command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cNextplayer(Player sender) {
		if (teamPlayers.size() == 0)
			return ERROR_COLOR + "There are no players set";

		nextPlayer(sender);
		return null;
	}

	/**
	 * Carry out the /prevplayer command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cPrevplayer(Player sender) {
		if (teamPlayers.size() == 0)
			return ERROR_COLOR + "There are no players set";

		prevPlayer(sender);
		return null;
	}

	/**
	 * Carry out the /nextwarp command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cNextwarp(Player sender) {
		if (warps.size() == 0)
			return ERROR_COLOR + "There are no warps set";

		nextWarp(sender);
		return null;
	}

	/**
	 * Carry out the /prevwarp command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cPrevwarp(Player sender) {
		if (warps.size() == 0)
			return ERROR_COLOR + "There are no warps set";

		prevWarp(sender);
		return null;
	}

	/**
	 * Carry out the /warp command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cWarp(Player sender, String[] args) {
		if (args.length == 0)
			return ERROR_COLOR + "You didn't specify the warpname: /warp [name]" + "\n" + ERROR_COLOR + "Existing warps: " + getAllWarpNames();

		CotmWarp warp = getWarpByName(args[0]);
		if (warp == null)
			warp = getWarpByNamePart(args[0]);

		if (warp == null)
			return ERROR_COLOR + "Warp " + args[0] + " doesn't exist!" + "\n" + ERROR_COLOR + "Existing warps: " + getAllWarpNames();

		saveTpLocation(sender);
		warp.warp(sender);
		return null;
	}

	/**
	 * Carry out the /tpback command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cTpback(Player sender, String[] args) {
		CotmPlayer player;
		if (args.length > 0) {
			// Tpback a specific player
			Player p = getServer().getPlayer(args[0]);
			if (p == null)
				return ERROR_COLOR + "Player " + args[0] + " is not registered; cannot tpback";

			player = getCotmPlayer(p);

			if (player == null)
				return ERROR_COLOR + "Player " + args[0] + " not found";

			if (player.tpBack()) {
				return OK_COLOR + "Player " + p.getName() + " was teleported back";
			} else {
				return ERROR_COLOR + "Player " + p.getName() + " was not previously teleported: cannot tpback";
			}

		} else {
			player = getCotmPlayer(sender);
			if (player == null)
				return ERROR_COLOR + "You are not registered; cannot tpback";

			if (player.tpBack())
				return null;
			else
				return ERROR_COLOR + "You were not previously teleported: cannot tpback";
		}
	}

	/**
	 * Carry out the /tphere command
	 * 
	 * @param sender the sender of the command
	 * @param args arguments
	 * @return response
	 */
	private String cTphere(Player sender, String[] args) {
		if (args.length != 1)
			return ERROR_COLOR + "You haven't specified a player name.";

		Player p = getServer().getPlayer(args[0]);

		if (p == null)
			return ERROR_COLOR + "Player " + args[0] + " has not been found on the server.";

		doTeleport(p, sender);
		return OK_COLOR + p.getName() + " has been teleported to your location!";

	}

	/**
	 * Carry out the /tpn command
	 * 
	 * @param sender the sender of the command
	 * @return response
	 */
	private String cTpn(Player sender) {
		if (lastNotifierLocation == null)
			return ERROR_COLOR + "Nobody has notified the CotM staff.";

		doTeleport(sender, lastNotifierLocation);
		return null;
	}

	/**
	 * Carry out the /tpd command
	 * 
	 * @param sender the sender of the command
	 * @return response
	 */
	private String cTpd(Player sender) {
		if (lastDeathLocation == null)
			return ERROR_COLOR + "Nobody has died.";

		doTeleport(sender, lastDeathLocation, "Teleported to death point of " + lastDeathPlayerName);
		return null;
	}

	/**
	 * Carry out the /tpl command
	 * 
	 * @param sender the sender of the command
	 * @return response
	 */
	private String cTpl(Player sender) {
		if (lastLogoutLocation == null)
			return ERROR_COLOR + "Nobody has logged out.";

		doTeleport(sender, lastLogoutLocation);
		return null;
	}

	/**
	 * Carry out the /tp command
	 * 
	 * @param sender the sender of the command
	 * @param args command arguments
	 * @return response
	 */
	private String cTp(Player sender, String[] args) {
		if (args.length == 0) {
			if (lastEventLocation == null)
				return ERROR_COLOR + "You haven't specified to who you want to teleport.";

			doTeleport(sender, lastEventLocation);
			return null;
		}

		if (args.length == 1) {
			// Teleport sender to target
			Player target = getServer().getPlayer(args[0]);
			if (target == null)
				return ERROR_COLOR + "Player " + args[0] + " has not been found on the server.";

			doTeleport(sender, target);
			return null;
		}

		if (args.length == 2) {
			// Teleport p1 to p2
			Player p1 = getServer().getPlayer(args[0]);
			Player p2 = getServer().getPlayer(args[1]);
			if (p1 == null)
				return ERROR_COLOR + "Player " + args[0] + " has not been found on the server.";
			if (p2 == null)
				return ERROR_COLOR + "Player " + args[1] + " has not been found on the server.";
	
			doTeleport(p1, p2);
			return OK_COLOR + p1.getName() + " has been teleported!";
		}

		if (args.length == 3 || args.length == 4) {
			// Teleport to x,y,z coordinates
			double x,y,z;
			Player p;
			try {
				if (args.length == 3) {
					x = Double.parseDouble(args[0]);
					y = Double.parseDouble(args[1]);
					z = Double.parseDouble(args[2]);
					p = sender;
				} else {
					x = Double.parseDouble(args[1]);
					y = Double.parseDouble(args[2]);
					z = Double.parseDouble(args[3]);
					p = getServer().getPlayer(args[0]);
					if (p == null)
						return ERROR_COLOR + "Player " + args[0] + " has not been found on the server.";
				}
			} catch (NumberFormatException e) {
				return ERROR_COLOR + "Invalid arguments to tp command";
			}
			
			if (y > 256 || y < 0)
				return ERROR_COLOR + "Y coordinate out of bounds";
			
			doTeleport(p, new Location(p.getWorld(), x, y, z));
			return null;
			
		}
		
		return ERROR_COLOR + "Too many arguments";
	}

	/**
	 * Carry out the /trophytp command
	 * 
	 * @return response
	 */
	private String cTrophytp() {
		if (gameType != GameType.CHAMPIONSHIP_MATCH)
			return ERROR_COLOR + "This command is only for championship matches!";

		if (trophy == null)
			return ERROR_COLOR + "The trophy hasn't been created yet with /trophy!";

		// tp the 'losers'
		for (Player p : world.getPlayers()) {
			if (!isChampion(p) && p.getGameMode() != GameMode.CREATIVE) {
				loser(p);
			}
		}
		// tp the champions
		getServer().getScheduler().scheduleAsyncDelayedTask(this, new Runnable() {
			public void run() {
				for (Player p : world.getPlayers()) {
					if (isChampion(p)) {
						champion(p);
					}
				}
			}
		}, 20L * 5);

		// 3 lighting strikes
		world.setTime(20L * 60 * 15); // set to night
		getServer().getScheduler().scheduleAsyncDelayedTask(this, new Runnable() {
			public void run() {
				world.strikeLightningEffect(trophy.getLightningLocation());
			}
		}, 20L * 5);

		getServer().getScheduler().scheduleAsyncDelayedTask(this, new Runnable() {
			public void run() {
				world.strikeLightningEffect(trophy.getLightningLocation());
			}
		}, 20L * 7);

		getServer().getScheduler().scheduleAsyncDelayedTask(this, new Runnable() {
			public void run() {
				world.strikeLightningEffect(trophy.getLightningLocation());
			}
		}, 20L * 9);

		getServer().getScheduler().scheduleAsyncDelayedTask(this, new Runnable() {
			public void run() {
				world.setTime(0);// set time back to day
				LoserDeath ld = LoserDeath.getRandomDeath(trophy.getPlatformLocation());
				ld.killLosers();
				getServer().broadcast(ld.getMessage(), Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
			}
		}, 20L * 13);

		return OK_COLOR + "The 'losers' are teleported near the trophy. The champions will arrive soon.";

	}

	/**
	 * Carry out the /trophy command
	 * 
	 * @return command response
	 */
	private String cTrophy() {
		if (gameType != GameType.CHAMPIONSHIP_MATCH)
			return ERROR_COLOR + "The trophy is only for championship matches!";
		// A champion is required
		if (champions == null)
			return ERROR_COLOR + "You have not said who won the game with /gg!";

		// Create or re-create the trophy
		if (trophy == null) {
			trophy = new Trophy(world, champions.getVictories());
		} else {
			trophy.reset(world, champions.getVictories());
		}

		trophy.build();
		return OK_COLOR + "The trophy has been created at spawn.";
	}

	/**
	 * Carry out the /delteam command
	 * 
	 * @param args arguments
	 */
	private String cDelteam(String[] args) {
		if (args.length == 0)
			return ERROR_COLOR + "You need to pass a suffix.";

		Team t = getTeamBySuffix(args[0]);
		if (t != null) {
			for (CotmPlayer cp : t.getTeamMembers()) {
				if (cp != null)
					cotmPlayers.remove(cp.getName().toLowerCase());
			}
			teams.remove(t);
			return OK_COLOR + "The team is deleted.";
		} else {
			return ERROR_COLOR + "The requested team was not found";
		}
	}

	/**
	 * Carry out the /clearteams command
	 * 
	 * @return
	 */
	private String cClearteams() {
		teams.clear();
		clearTeamPlayers();
		return OK_COLOR + "All teams have been deleted!";
	}
	
	/**
	 * Carry out the /teaminfo command
	 * 
	 * @param args arguments
	 * @return response
	 */
	private String cTeaminfo(String[] args) {
		if (args.length == 0) {
			int s = teams.size();
			if (s == 0)
				return ERROR_COLOR + "There are no teams set";

			String response = "There " + (s == 1 ? "is 1 team" : "are " + s + " teams") + ":\n";
			for (Team t : teams) {
				response += t.getInfo() + "\n";
			}
			return response;
		}

		Team t = getTeamBySuffix(args[0]);
		if (t == null)
			return ERROR_COLOR + "There is no team with the suffix " + args[0];

		return t.getInfo();

	}

	
	
	/**
	 * Carry out the /gg command
	 * 
	 * @param sender the player who sent the command
	 * @param args arguments
	 * @return response to be displayed
	 */
	private String cGg(Player sender, String[] args) {
		if (!isRef(sender) && gameType != GameType.SCRIMMAGE)
			return (ERROR_COLOR + "You're not a referee!");
		if (!matchRunning())
			return (ERROR_COLOR + "The game has not been initiated with /match yet.");
		
		if (gameType == GameType.SCRIMMAGE) {
			// For scrims, the winning team is optional
			
			if (args.length > 0) {
				ggScrim(getTeamBySuffix(args[0]));
			} else {
				ggScrim();
			}
			return null;
		}
		
		if (args.length == 0)
			return (ERROR_COLOR + "You haven't specified the winner's suffix");
		
		Team winner = getTeamBySuffix(args[0]);
		if (winner == null)
			return (ERROR_COLOR + "There is no team with suffix " + args[0] + "!");
		
		gg(winner);
		
		return null;
	}

	/**
	 * Carry out the /dq command
	 * 
	 * @param sender the player who sent the command
	 * @param args arguments
	 * @return response to be displayed
	 */
	private String cDq(Player sender, String[] args) {
		if (!isRef(sender) && gameType != GameType.SCRIMMAGE)
			return (ERROR_COLOR + "You're not a referee!");
		if (!matchRunning())
			return (ERROR_COLOR + "The game has not been initiated with /match yet.");
		if (args.length == 0)
			return (ERROR_COLOR + "You haven't specified the disqualified team's suffix");
		Team disqualified = getTeamBySuffix(args[0]);
		if (disqualified == null)
			return (ERROR_COLOR + "There is no team with suffix " + args[0] + "!");

		
		if (gameType == GameType.SCRIMMAGE)
			ggScrim(this.getOtherTeam(disqualified));
		else
			ggDisqualify(disqualified);
				
		return null;
	}

	/**
	 * Load predefined warning messages from the config file
	 */
	private void loadWarningMessages() {
		ConfigurationSection warnings = this.getConfig().getConfigurationSection("warnings");
		for (String s : warnings.getKeys(false))
			WarningMessages.put(s, warnings.getString(s));
	}
	
	/**
	 * Spawn a "special mob" at the given location
	 * 
	 * @param location the location where the creature will be spawned
	 */
	public void specialSpawn(Location location) {
		int index = r.nextInt(SPECIAL_MOBS.length);
		SPECIAL_MOBS[index].spawn(location, getServer());
	}

	/**
	 * Initiates the countdown
	 * 
	 * @param gameTime the time to set in-game when the match begins
	 * @param countdownLength the number of seconds to count down
	 */
	public void startCountdown(Long gameTime, Integer countdownLength) {
		time = gameTime;
		start = null;
		champions = null;
		disqualification = false;

		getServer().broadcastMessage(ChatColor.DARK_PURPLE + "Hunger, Health, Time and Butcher will be done at GO!");

		announceGameDetails(Server.BROADCAST_CHANNEL_USERS);

		countdownover = countdownLength + 4 - (countdownLength % 5);
		if (countdownover < 10) {
			rfw(time, matchButcherType);

		} else {
			getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
				public void run() {
					rfw(time, matchButcherType);
				}
			}, 20L * (countdownover - 10));
		}
		countdown();
	}

	/**
	 * Clears all CotmPlayers who are not staff
	 */
	public void clearTeamPlayers() {
		for (CotmPlayer cp : cotmPlayers.values()) {
			if (cp.getPlayerType() == PlayerType.PLAYER)
				cotmPlayers.remove(cp.getName().toLowerCase());
		}
	}

	/**
	 * Check if everything is ready for a match to begin.
	 * 
	 * @param p the player who is asking
	 * @return whether everything is ready
	 */
	public boolean readyForGame(Player p) {

		// Always ready for a scrimmage!
		if (gameType == GameType.SCRIMMAGE)
			return true;

		// Otherwise, check settings.
		if (teams.size() == 2) {
			Team t1 = getTeam(0);
			Team t2 = getTeam(1);
			if (mapName == null) {
				p.sendMessage(ERROR_COLOR + "You need to set a map name with /map!");
			} else if (gameType == null) {
				p.sendMessage(ERROR_COLOR + "You need to set a gametype with /gametype!");
			} else if (gameType == GameType.CHAMPIONSHIP_MATCH && getTeam(0).getOldVictories() != 0 && getTeam(1).getOldVictories() != 0) {
				p.sendMessage(ERROR_COLOR + "This is a championship match. One team should have 0 victories. Use /teamvictories to fix this.");
			} else if (gameType == GameType.CHAMPIONSHIP_MATCH && (getTeam(0).getOldVictories() < 0 || getTeam(1).getOldVictories() < 0)) {
				p.sendMessage(ERROR_COLOR + "Please set the correct number of victories for the current champion using /teamvictories");
				p.sendMessage(ERROR_COLOR + "Use this file to get the correct number: " + CHALLENGER_LIST_URL);
			} else if (gameType == GameType.QUEUE_FIGHT && (getTeam(0).getOldVictories() < 0 || getTeam(1).getOldVictories() < 0)) {
				p.sendMessage(ERROR_COLOR + "Please set the correct number of points for the teams using /teampoints");
				p.sendMessage(ERROR_COLOR + "Use this file to get the correct number: " + CHALLENGER_LIST_URL);
			} else if ((gameType == GameType.CHAMPIONSHIP_MATCH || gameType == GameType.QUEUE_FIGHT) && numberOfRefsOnline() == 0) {
				p.sendMessage(ERROR_COLOR + "There is no referee registered. Use /ref [playername] to set.");
			} else if (t1.numberOfPlayers() == 0) {
				p.sendMessage(ERROR_COLOR + "There is no player registered for " + t1.getTeamName() + "(" + t1.getSuffix() + ")" + "!");
			} else if (t2.numberOfPlayers() == 0) {
				p.sendMessage(ERROR_COLOR + "There is no player registered for " + t2.getTeamName() + "(" + t2.getSuffix() + ")" + "!");
			} else {
				return true;
			}
		} else if (teams.size() > 2) {
			p.sendMessage(ERROR_COLOR + "There are more than 2 teams registered! Please use /teamdel to delete teams, or /clearteams to start over.");
		} else if (teams.size() < 2) {
			p.sendMessage(ERROR_COLOR + "There are not enough teams registered! Register a team with /team or /addteam.");
		}
		return false;
	}

	/**
	 * Look up the Team which has the given suffix
	 * 
	 * @param suffix the team suffix
	 * @return matching team, or null if none found
	 */
	public Team getTeamBySuffix(String suffix) {
		for (Team t : teams) {
			if (t.getSuffix().equalsIgnoreCase(suffix))
				return t;
		}
		return null;
	}

	/**
	 * Look up the CotmPlayer with the given IGN
	 * 
	 * @param name the player's IGN
	 * @return matching CotmPlayer, or null if none found
	 */
	public CotmPlayer getCotmPlayer(String name) {
		if (cotmPlayers.containsKey(name.toLowerCase()))
			return cotmPlayers.get(name.toLowerCase());
		return null;
	}

	/**
	 * Look up the CotmPlayer for the given Player
	 * 
	 * @param p the Player object
	 * @return the matching CotmPlayer, or null if none found
	 */
	public CotmPlayer getCotmPlayer(Player p) {
		return getCotmPlayer(p.getName());
	}

	/**
	 * Add the given CotmPlayer. If a player with the same name exists, they
	 * will be replaced.
	 * 
	 * @param cp player to be (re)added
	 */
	public void addCotmPlayer(CotmPlayer cp) {
		CotmPlayer cp2 = getCotmPlayer(cp.getName());
		if (cp2 != null)
			cotmPlayers.remove(cp2.getName().toLowerCase());
		cotmPlayers.put(cp.getName().toLowerCase(), cp);
	}

	/**
	 * Remove a CotmPlayer by name
	 * 
	 * @param s The name of the player to remove
	 */
	public void removeCotmPlayer(String s) {
		CotmPlayer cp2 = getCotmPlayer(s);
		if (cp2 != null)
			cotmPlayers.remove(cp2.getName().toLowerCase());
	}

	/**
	 * Look up a warp by exact name match
	 * 
	 * @param name name of the warp
	 * @return the CotmWarp, if found, or null
	 */
	public CotmWarp getWarpByName(String name) {
		for (CotmWarp w : warps) {
			if (w.getName().equalsIgnoreCase(name))
				return w;
		}
		return null;
	}

	/**
	 * Look up a warp by partial name match
	 * 
	 * @param name name of the warp
	 * @return the CotmWarp, if found, or null
	 */
	public CotmWarp getWarpByNamePart(String name) {
		for (CotmWarp w : warps) {
			if (w.getName().toLowerCase().startsWith(name.toLowerCase()))
				return w;
		}
		return null;
	}

	/**
	 * Teleport the player to the previous player in the cycle.
	 * 
	 * @param p the player to teleport
	 * @return The player teleported to
	 */
	public void prevPlayer(Player p) {
		CotmPlayer cp = getCotmPlayer(p);
		if (teamPlayers.size() == 0 || cp == null) return;
		
		Player destination = null;
		
		if (cp.getTeamIndex() == 0) {
			// Cycle all players
			int startingPlayerIndex = cp.prevPlayerIndex(teamPlayers.size());
			int index = startingPlayerIndex;
			destination = teamPlayers.get(index).getPlayer();

			while (destination == null) {
				index = cp.prevPlayerIndex(teamPlayers.size());
				if (index == startingPlayerIndex)
					return;
				destination = teamPlayers.get(index).getPlayer();
			}

			
		} else {
			// cycle within team
			int startingPlayerIndex = cp.prevPlayerIndex(teamPlayers.size());
			int index = startingPlayerIndex;
			destination = teamPlayers.get(index).getPlayer();

			while (destination == null || !teams.get(cp.getTeamIndex()-1).isTeamMember(destination)) {
				index = cp.prevPlayerIndex(teamPlayers.size());
				if (index == startingPlayerIndex)
					return;
				destination = teamPlayers.get(index).getPlayer();
			}

		}
		saveTpLocation(p);
		doTeleport(p, destination, "Teleported to " + destination.getDisplayName());
	}
	

	/**
	 * Teleport the player to the next player in the cycle.
	 * 
	 * @param p the player to teleport
	 * @return The player teleported to
	 */
	public void nextPlayer(Player p) {
		CotmPlayer cp = getCotmPlayer(p);
		if (teamPlayers.size() == 0 || cp == null) return;

		Player destination = null;
		
		if (cp.getTeamIndex() == 0) {
			// Cycle all players
			
			int startingPlayerIndex = cp.nextPlayerIndex(teamPlayers.size());
			int index = startingPlayerIndex;
			destination = teamPlayers.get(index).getPlayer();

			while (destination == null) {
				index = cp.nextPlayerIndex(teamPlayers.size());
				if (index == startingPlayerIndex)
					return;
				destination = teamPlayers.get(index).getPlayer();
			}

			
		} else {
			// cycle within team
			int startingPlayerIndex = cp.nextPlayerIndex(teamPlayers.size());
			int index = startingPlayerIndex;
			destination = teamPlayers.get(index).getPlayer();

			while (destination == null || !teams.get(cp.getTeamIndex()-1).isTeamMember(destination)) {
				index = cp.nextPlayerIndex(teamPlayers.size());
				if (index == startingPlayerIndex)
					return;
				destination = teamPlayers.get(index).getPlayer();
			}

		}
		saveTpLocation(p);
		doTeleport(p, destination, "Teleported to " + destination.getDisplayName());
	}

	/**
	 * Set a new team to follow with next/previous player commands
	 * 
	 * @param sender The player sending the command
	 * @param toFollow The team to be followed, or null for no team
	 */
	private void setFollowedTeam(Player sender, Team toFollow) {
		if (toFollow == null) {
			this.setFollowedTeam(sender, 0);
			return;
		}
		
		int teamIndex = this.teams.indexOf(toFollow)+1;
		if (teamIndex>0) {
			this.setFollowedTeam(sender, teamIndex);
		}
	}

	/**
	 * Set a new team to follow with next/previous player commands
	 * 
	 * @param sender The player sending the command
	 * @param followedTeamIndex The index of the team to be followed. 0 is all teams, 1, 2 etc are teams.
	 */
	private void setFollowedTeam(Player sender, int followedTeamIndex) {
		CotmPlayer cp = this.getCotmPlayer(sender);
		if (cp != null)
			cp.setTeamIndex(followedTeamIndex);
	}
	
	
	/**
	 * Move to the previous team in the cycle
	 * 
	 * @param sender The player sending the command
	 */
	private void prevTeam(Player sender) {
		CotmPlayer cp = this.getCotmPlayer(sender);
		if (cp != null) {
			int team = cp.prevTeamIndex(teams.size());
			if (team>0) 
				sender.sendMessage(OK_COLOR + "Now following " + teams.get(team-1).getColoredTeamName());
			else
				sender.sendMessage(OK_COLOR + "Now following all teams");
		}
	}

	/**
	 * Move to the next team in the cycle
	 * 
	 * @param sender The player sending the command
	 */
	private void nextTeam(Player sender) {
		CotmPlayer cp = this.getCotmPlayer(sender);
		if (cp != null) {
			int team = cp.nextTeamIndex(teams.size());
			if (team>0) 
				sender.sendMessage(OK_COLOR + "Now following " + teams.get(team-1).getColoredTeamName());
			else
				sender.sendMessage(OK_COLOR + "Now following all teams");
		}
	}

	
	/**
	 * Warp the player to the previous warp in the cycle
	 * 
	 * @param p the player to warp
	 */
	public void prevWarp(Player p) {
		CotmPlayer cp = getCotmPlayer(p);
		if (warpCycle.size() > 0 && cp != null) {
			int index = cp.prevWarpIndex(warpCycle.size());
			saveTpLocation(p);
			warpCycle.get(index).warp(p);
		}
	}

	/**
	 * Warp the player to the next warp in the cycle
	 * 
	 * @param p the player to warp
	 */
	public void nextWarp(Player p) {
		CotmPlayer cp = getCotmPlayer(p);
		if (warpCycle.size() > 0 && cp != null) {
			int index = cp.nextWarpIndex(warpCycle.size());
			saveTpLocation(p);
			warpCycle.get(index).warp(p);
		}
	}

	/**
	 * Get a list of available warps
	 * 
	 * @return comma-separated list of warp names
	 */
	public String getAllWarpNames() {
		String s = "";
		for (CotmWarp w : warps) {
			s += w.getName() + ", ";
		}
		if (s.length() >= 2)
			return s.substring(0, s.length() - 2);
		else
			return s;
	}

	/**
	 * Teleport one player to another. If player is a staff member, fancy
	 * teleport will be done. Adds a custom message to be displayed.
	 * 
	 * @param p1 player to be teleported
	 * @param p2 player to be teleported to
	 * @param message the message to be displayed
	 */
	public void doTeleport(Player p1, Player p2, String message) {
		saveTpLocation(p1);

		// if the first player is a staff member, do fancy teleport. Otherwise
		// just
		// teleport.
		if (!isStaff(p1))
			p1.teleport(p2);
		else
			doFancyTeleport(p1, p2);

		if (p1.getGameMode() == GameMode.CREATIVE)
			p1.setFlying(true);

		if (message != null && !message.isEmpty())
			p1.sendMessage(OK_COLOR + message);
	}

	/**
	 * Teleport one player to another. If player is a staff member, fancy
	 * teleport will be done.
	 * 
	 * @param p1 player to be teleported
	 * @param p2 player to be teleported to
	 */
	public void doTeleport(Player p1, Player p2) {
		this.doTeleport(p1, p2, "You have been teleported!");
	}

	/**
	 * Teleport a player to a specific location. Adds a custom message to be displayed.
	 * 
	 * @param p1 player to be teleported
	 * @param l location to be teleported to
	 * @param message the message to be displayed
	 */
	public void doTeleport(Player p1, Location l, String message) {
		saveTpLocation(p1);
		p1.teleport(l);
		if (p1.getGameMode() == GameMode.CREATIVE)
			p1.setFlying(true);
		p1.sendMessage(OK_COLOR + message);
	}
	
	/**
	 * Teleport a player to a specific location
	 * 
	 * @param p1 player to be teleported
	 * @param l location to be teleported to
	 */
	public void doTeleport(Player p1, Location l) {
		this.doTeleport(p1, l, "You have been teleported!");
	}

	/**
	 * Save a player's location prior to tp/warp, so that it can be retrieved
	 * for /tpback
	 * 
	 * @param p player whose location is to be saved
	 */
	public void saveTpLocation(Player p) {
		CotmPlayer player = getCotmPlayer(p);
		if (player != null)
			player.setTpBackLocation(p.getLocation());
	}

	/**
	 * Teleports a player NEAR to another player
	 * 
	 * If possible, they will be placed 5 blocks away, facing towards the
	 * destination player.
	 * 
	 * @param streamer the Player who will be fancy-teleported
	 * @param p the Player they are to be teleported to
	 */
	public void doFancyTeleport(Player streamer, Player p) {
		Location l = p.getLocation();

		Location lp = new Location(l.getWorld(), l.getX(), l.getY(), l.getZ());
		Location lxp = new Location(l.getWorld(), l.getX(), l.getY(), l.getZ());
		Location lxn = new Location(l.getWorld(), l.getX(), l.getY(), l.getZ());
		Location lzp = new Location(l.getWorld(), l.getX(), l.getY(), l.getZ());
		Location lzn = new Location(l.getWorld(), l.getX(), l.getY(), l.getZ());
		Location tpl = new Location(l.getWorld(), l.getX(), l.getY(), l.getZ());
		boolean xp = true, xn = true, zp = true, zn = true;

		for (int i = 0; i < 5; i++) {
			if (xp) {
				lxp.setX(lxp.getX() + 1);
				if (!isSpaceForPlayer(lxp))
					xp = false;
			}
			if (xn) {
				lxn.setX(lxn.getX() - 1);
				if (!isSpaceForPlayer(lxn))
					xn = false;
			}
			if (zp) {
				lzp.setZ(lzp.getZ() + 1);
				if (!isSpaceForPlayer(lzp))
					zp = false;
			}
			if (zn) {
				lzn.setZ(lzn.getZ() - 1);
				if (!isSpaceForPlayer(lzn))
					zn = false;
			}
		}

		if (!xp)
			lxp.setX(lxp.getX() - 1);
		if (!xn)
			lxn.setX(lxn.getX() + 1);
		if (!zp)
			lzp.setZ(lzp.getZ() - 1);
		if (!zn)
			lzn.setZ(lzn.getZ() + 1);

		tpl.setYaw(90);
		tpl.setPitch(0);

		if (lxp.distanceSquared(lp) > tpl.distanceSquared(lp)) {
			tpl = lxp;
			tpl.setYaw(90);
		}
		if (lxn.distanceSquared(lp) > tpl.distanceSquared(lp)) {
			tpl = lxn;
			tpl.setYaw(270);
		}
		if (lzp.distanceSquared(lp) > tpl.distanceSquared(lp)) {
			tpl = lzp;
			tpl.setYaw(180);
		}
		if (lzn.distanceSquared(lp) > tpl.distanceSquared(lp)) {
			tpl = lzn;
			tpl.setYaw(0);
		}
		streamer.teleport(tpl);
	}

	/**
	 * Calculates if it's possible for a player to fit in a certain spot.
	 * 
	 * @param feetLocation the location where the feet should be
	 * @return wheter or not there's place for both his feet and head
	 */
	public static boolean isSpaceForPlayer(Location feetLocation) {
		World w = feetLocation.getWorld();
		int x = feetLocation.getBlockX(), y = feetLocation.getBlockY(), z = feetLocation.getBlockZ();
		Block b1 = w.getBlockAt(x, y, z);
		Block b2 = w.getBlockAt(x, y + 1, z);
		return isSpaceForPlayer(b1) && isSpaceForPlayer(b2);
	}

	/**
	 * Calculates if it's possible for a player to fit in a certain spot.
	 * 
	 * @param w the world in which we need to check if there's space for the
	 *            player
	 * @param x the x coordinate of the block to check
	 * @param y the y coordinate of the block (at the feet) to check
	 * @param z the z coordinate of the block to check
	 * @return whether or not there's place for both his feet and head
	 */
	public static boolean isSpaceForPlayer(World w, int x, int y, int z) {
		Block b1 = w.getBlockAt(x, y, z);
		Block b2 = w.getBlockAt(x, y + 1, z);
		return isSpaceForPlayer(b1) && isSpaceForPlayer(b2);
	}

	/**
	 * Determine whether a given block is a either empty or liquid (but not
	 * lava)
	 * 
	 * @param b the block to check
	 * @return whether the block is suitable
	 */
	public static boolean isSpaceForPlayer(Block b) {
		return (b.isEmpty() || b.isLiquid()) && b.getType() != Material.LAVA && b.getType() != Material.STATIONARY_LAVA;
	}

	public boolean teamExists(String suffix) {
		return getTeamBySuffix(suffix) != null;
	}

	public Team getChallengers() {
		for (Team t : teams) {
			if (t.isChallenger())
				return t;
		}
		return null;
	}

	public int numberOfChallengers() {
		int n = 0;
		for (Team t : teams) {
			if (t.isChallenger())
				n++;
		}
		return n;
	}

	/**
	 * Output the match title, streamers, referees and teams to the specified
	 * channel
	 * 
	 * @param channel The channel for the announcement
	 */
	public void announceGameDetails(String channel) {
		Server s = getServer();

		// map name + game type
		s.broadcast(MAIN_COLOR + "This is " + SIDE_COLOR + getGameTitle(), channel);

		// streamers
		if (numberOfStreamersOnline() == 1) {
			s.broadcast(SIDE_COLOR + getStreamersByName() + MAIN_COLOR + " is today's caster", channel);
		} else if (numberOfStreamersOnline() > 1) {
			s.broadcast(SIDE_COLOR + getStreamersByName() + MAIN_COLOR + " are today's casters", channel);
		}

		// refs
		if (numberOfRefsOnline() == 1) {
			s.broadcast(SIDE_COLOR + getRefsByName() + MAIN_COLOR + " is the referee for this match", channel);
		} else if (numberOfRefsOnline() > 1) {
			s.broadcast(SIDE_COLOR + getRefsByName() + MAIN_COLOR + " are the referees for this match", channel);
		}

		// teams
		Team t1 = getTeam(0);
		Team t2 = getTeam(1);
		if (gameType == GameType.CHAMPIONSHIP_MATCH) {
			Team challengers = getChallengers();
			Team defenders = getOtherTeam(challengers);
			if (challengers == null || defenders == null) {
				// Victories aren't initiated yet, so challengers couldn't be
				// found.
				s.broadcast("" + t1 + MAIN_COLOR + " vs. " + t2, channel);
			} else if (challengers.getOldVictories() == 0 && defenders.getOldVictories() == 0) {
				// Both teams are on 0 points, so just display team names
				s.broadcast("" + t1 + MAIN_COLOR + " vs. " + t2, channel);
			} else {
				s.broadcast("" + challengers + MAIN_COLOR + " challenges " + defenders, channel);
			}
		} else if (gameType == GameType.QUEUE_FIGHT || gameType == GameType.MEET_THE_MAP || gameType == GameType.SCRIMMAGE) {
			if (t1 != null && t2 != null) {
				s.broadcast("" + t1 + MAIN_COLOR + " vs. " + t2, channel);
				if (t1.numberOfPlayers() > 0)
					s.broadcast(
							t1.getColor() + t1.getSuffix() + MAIN_COLOR + " is the team consisting of: " + t1.getColor() + t1.getTeamMembersByName(),
							channel);
				if (t2.numberOfPlayers() > 0)
					s.broadcast(
							t2.getColor() + t2.getSuffix() + MAIN_COLOR + " is the team consisting of: " + t2.getColor() + t2.getTeamMembersByName(),
							channel);
			}
		}
	}

	/**
	 * Announce how long the game has been going, in hh:mm:ss format, to the
	 * specified broadcast channel.
	 * 
	 * If the match is not running, nothing will happen.
	 * 
	 * @param channel The channel for the announcement.
	 */
	public void announceGameDuration(String channel) {
		// Output match duration
		if (isRunning()) {
			getServer().broadcast(MAIN_COLOR + "The game has been going for " + SIDE_COLOR + timespanHMS(start, Calendar.getInstance()), channel);
		}
	}

	public void announceStatsUrl(String channel) {
		if (uploadUrl != null) {
			getServer().broadcast(MAIN_COLOR + "Match statistics can be viewed at " + SIDE_COLOR + uploadUrl, channel);
		}

	}

	public void setVanish(Player p1) {
		if (world != null) {
			boolean hideP1;
			boolean hideP2;
			for (Player p2 : world.getPlayers()) {
				hideP1 = false;
				hideP2 = false;

				// See who needs to hide for who.
				if (isPlayer(p1)) {
					if (isStaff(p2))
						hideP2 = true;
				} else if (isStreamer(p1)) {
					if (isPlayer(p2))
						hideP1 = true;
					else if (!isStreamer(p2))
						hideP2 = true;

				} else {
					if (isPlayer(p2) || isStreamer(p2))
						hideP1 = true;
				}

				// Hide the players if necessary
				if (hideP1)
					p2.hidePlayer(p1);
				else
					p2.showPlayer(p1);
				if (hideP2)
					p1.hidePlayer(p2);
				else
					p1.showPlayer(p2);
			}

			if (isStaff(p1)) {
				p1.setAffectsSpawning(false);
				p1.setCollidesWithEntities(false);
				p1.sendMessage(OK_COLOR + "You're now vanished.");
			} else {
				p1.setAffectsSpawning(true);
				p1.setCollidesWithEntities(true);
			}
		}
	}

	public void unvanish(Player vanisher) {
		if (world != null) {
			for (Player other : world.getPlayers()) {
				other.showPlayer(vanisher);
			}

			vanisher.setAffectsSpawning(true);
			vanisher.setCollidesWithEntities(true);
			vanisher.sendMessage(OK_COLOR + "You are now unvanished!");
		}
	}

	public void hideStaffFor(Player player) {
		for (Player staff : getAllStaffMembers()) {
			player.hidePlayer(staff);
		}
	}

	public void rfw(long time, ButcherType butcher) {

		// Do not do anything if countdown has been cancelled
		if (countdownover == -1) return;
		
		renewAll();
		setTime(time);
		butcher(butcher);
		clearDrops();

		for (Player p : getAllStaffMembers()) {
			setVanish(p);
		}

		setTeamPlayers();
	}

	/**
	 * Generates the teamPlayers list for cycling through players
	 * 
	 */
	public void setTeamPlayers() {
		teamPlayers.clear();
		for (Team t : teams) {
			for (CotmPlayer cp : t.getTeamMembers()) {
				teamPlayers.add(cp);
			}
		}
	}

	public void minuteAlerts() {
		announceGameDetails(Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
		int t = timespanNearestMinute(start, Calendar.getInstance());
		getServer().broadcastMessage(MAIN_COLOR + "The game has been going for " + SIDE_COLOR + t + " minute" + ((t != 1) ? "s" : ""));
	}

	public void scheduleMinuteAlerts() {
		cancelMinuteAlerts();
		minuteAlertsScheduler = getServer().getScheduler().scheduleAsyncRepeatingTask(this, new Runnable() {
			public void run() {
				minuteAlerts();
			}
		}, 20L * 60 * MINUTE_ALERTS, 20L * 60 * MINUTE_ALERTS);

		endReminderScheduler = getServer().getScheduler().scheduleAsyncDelayedTask(this, new Runnable() {
			public void run() {
				getServer().broadcast(ALERT_COLOR + "Staff alert: 10 minutes match time remaining", Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
			}
		}, 20L * 60 * 110);
	}

	/**
	 * Cancel the scheduler for game duration and game ending alerts
	 */
	public void cancelMinuteAlerts() {
		if (minuteAlertsScheduler != 0)
			getServer().getScheduler().cancelTask(minuteAlertsScheduler);
		if (endReminderScheduler != 0)
			getServer().getScheduler().cancelTask(endReminderScheduler);
	}

	public void countdown() {
		if (countdownover > 0) {
			if (countdownover % 5 == 0 || countdownover <= 5)
				getServer().broadcastMessage(COUNTDOWN_COLOR + "The game starts in " + COUNTDOWN_SIDE_COLOR + countdownover + COUNTDOWN_COLOR +" seconds.");
			countdownover--;
			getServer().getScheduler().scheduleAsyncDelayedTask(this, new Runnable() {
				public void run() {
					countdown();
				}
			}, 20L);
		} else if (countdownover == 0) {
			getServer().broadcastMessage(MAIN_COLOR + "GO!");
			start = Calendar.getInstance();
			scheduleMinuteAlerts();
			resetPlayerStats();
			uploadUrl = null;
		}
	}

	public void countdownSumo() {
		if (countdownoverSumo == 5) {
			renewAll();
		}

		if (countdownoverSumo > 0) {
			if (countdownoverSumo % 5 == 0 || countdownoverSumo <= 5)
				getServer().broadcastMessage(COUNTDOWN_COLOR + "The sumo starts in " + COUNTDOWN_SIDE_COLOR + countdownoverSumo + COUNTDOWN_COLOR + " seconds.");
			countdownoverSumo--;
			getServer().getScheduler().scheduleAsyncDelayedTask(this, new Runnable() {
				public void run() {
					countdownSumo();
				}
			}, 20L);
		} else if (countdownoverSumo == 0) {
			getServer().broadcastMessage(MAIN_COLOR + "GO!");
		}
	}

	/**
	 * Check if the match is running
	 * 
	 * @return Whether the match is underway
	 */
	public boolean matchRunning() {
		return ((this.start != null) && (this.end == null));
	}
	
	public void setTime(long time) {
		world.setTime(time);
		getServer().broadcastMessage(OK_COLOR + "Time has been set!");
	}

	public void renew(Player p) {
		heal(p);
		feed(p);
		clearXP(p);
		clearPotionEffects(p);
		if (p.getGameMode() == GameMode.SURVIVAL)
			clearInventory(p);
	}

	public void renewAll() {
		for (Player p : world.getPlayers()) {
			renew(p);
			p.sendMessage(OK_COLOR + "You have been healed and fed!");
		}
	}

	public void heal(Player p) {
		p.setHealth(20);
	}

	public void feed(Player p) {
		p.setFoodLevel(20);
		p.setExhaustion(0.0F);
		p.setSaturation(5.0F);
	}

	public void clearXP(Player p) {
		p.setTotalExperience(0);
		p.setExp(0);
		p.setLevel(0);
	}

	public void clearPotionEffects(Player p) {
		for (PotionEffect pe : p.getActivePotionEffects()) {
			p.removePotionEffect(pe.getType());
		}
	}

	public void clearInventory(Player player) {
		PlayerInventory i = player.getInventory();
		i.clear();
		i.setHelmet(null);
		i.setChestplate(null);
		i.setLeggings(null);
		i.setBoots(null);
	}

	public void butcher(ButcherType b) {
		switch (b) {
		case BUTCHER_HOSTILE:
			butcherHostile();
			break;
		case BUTCHER_FRIENDLY:
			butcherFriendly();
			break;
		case BUTCHER_ALL:
			butcherAll();
			break;
		}
	}

	public void butcherAll() {
		for (Entity entity : world.getEntitiesByClass(LivingEntity.class)) {
			if (!(entity instanceof HumanEntity))
				entity.remove();
		}
		getServer().broadcastMessage(OK_COLOR + "All mobs have been removed!");
	}

	public void clearDrops() {
		for (Entity entity : world.getEntitiesByClass(Item.class)) {
			entity.remove();
		}
		getServer().broadcastMessage(OK_COLOR + "All drops have been removed!");
	}

	public void butcherFriendly() {
		for (Entity entity : world.getEntitiesByClass(LivingEntity.class)) {
			if (entity instanceof Animals || entity instanceof Squid || entity instanceof Golem || entity instanceof Villager)
				entity.remove();
		}
		getServer().broadcastMessage(OK_COLOR + "All friendly mobs have been removed!");
	}

	public void butcherHostile() {
		for (Entity entity : world.getEntitiesByClass(LivingEntity.class)) {
			if (entity instanceof Monster || entity instanceof MagmaCube || entity instanceof Slime || entity instanceof EnderDragon
					|| entity instanceof Ghast)
				entity.remove();
		}
		getServer().broadcastMessage(OK_COLOR + "All hostile mobs have been removed!");
	}

	public int timespan(Calendar c1, Calendar c2) {
		return (int) (c2.getTimeInMillis() - c1.getTimeInMillis()) / 1000;
	}

	public int timespanNearestMinute(Calendar c1, Calendar c2) {
		return (int) (Math.floor(timespan(c1, c2) + 30) / 60);
	}

	public String timespanHMS(Calendar c1, Calendar c2) {
		String s = "";

		// seconds
		Double d = (double) timespan(c1, c2);
		int dS = remainder(d, 60);

		// minutes
		d = Math.floor(d / 60);
		int dM = remainder(d, 60);

		// hours
		d = Math.floor(d / 60);
		int dH = remainder(d, 24);

		// The string
		if (dH <= 9)
			s += "0";
		s += dH + ":";
		if (dM <= 9)
			s += "0";
		s += dM + ":";
		if (dS <= 9)
			s += "0";
		s += dS;
		return s;
	}

	public int remainder(Double d, int divisor) {
		return ((int) (d - (Math.floor((d / divisor)) * divisor)));
	}

	public void setOp(Player player, boolean op) {
		if (player.isOp() != op) {
			player.setOp(op);
			if (op) {
				player.sendMessage(ChatColor.YELLOW + "You are now op!");
			} else {
				player.sendMessage(ChatColor.YELLOW + "You are no longer op!");
			}
		}
	}

	public boolean isStaffMember(Player p, PlayerType pt) {
		CotmPlayer cp = getCotmPlayer(p);
		if (cp != null)
			return cp.getPlayerType() == pt;
		else
			return false;
	}

	public boolean isChampion(Player p) {
		return champions.isTeamMember(p);
	}

	public boolean isRef(Player p) {
		return isStaffMember(p, PlayerType.REF);
	}

	public boolean isStreamer(Player p) {
		return isStaffMember(p, PlayerType.STREAMER);
	}

	public boolean isAssistant(Player p) {
		return isStaffMember(p, PlayerType.ASSISTANT);
	}

	public boolean isStaff(Player p) {
		return isRef(p) || isStreamer(p) || isAssistant(p);
	}

	public boolean isPlayer(Player p) {
		CotmPlayer cp = getCotmPlayer(p);
		if (cp != null)
			return cp.isPlayer();
		else
			return true;
	}

	public int numberOfStaffMembersOnline(PlayerType pt) {
		int numberOfPlayers = 0;
		for (Player p : world.getPlayers()) {
			if (isStaffMember(p, pt))
				numberOfPlayers++;
		}
		return numberOfPlayers;
	}

	public int numberOfRefsOnline() {
		return numberOfStaffMembersOnline(PlayerType.REF);
	}

	public int numberOfStreamersOnline() {
		return numberOfStaffMembersOnline(PlayerType.STREAMER);
	}

	/**
	 * This makes a player a champion on /trophytp. It teleports them and give
	 * them the right stuff
	 * 
	 * @param p The player that is a champion
	 */
	public void champion(Player p) {
		p.teleport(trophy.getChampionLocation());
		p.setGameMode(GameMode.SURVIVAL);
		p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20 * 60 * 10, 10));
		clearInventory(p);
		PlayerInventory pi = p.getInventory();
		// enchanted diamond sword
		ItemStack is = new ItemStack(Material.DIAMOND_SWORD);
		is.addEnchantment(Enchantment.DAMAGE_ALL, 5);
		is.addEnchantment(Enchantment.FIRE_ASPECT, 2);
		is.addEnchantment(Enchantment.KNOCKBACK, 2);
		pi.setItem(0, is);
		// enchanted bow
		is = new ItemStack(Material.BOW);
		is.addEnchantment(Enchantment.ARROW_DAMAGE, 5);
		is.addEnchantment(Enchantment.ARROW_FIRE, 1);
		is.addEnchantment(Enchantment.ARROW_KNOCKBACK, 2);
		is.addEnchantment(Enchantment.ARROW_INFINITE, 1);
		pi.setItem(1, is);
		// arrow
		is = new ItemStack(Material.ARROW, 64);
		pi.setItem(2, is);
		// OP armor
		is = new ItemStack(Material.DIAMOND_BOOTS);
		is.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		is.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 4);
		is.addEnchantment(Enchantment.PROTECTION_FIRE, 4);
		is.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 4);
		is.addEnchantment(Enchantment.PROTECTION_FALL, 4);
		pi.setBoots(is);
		is = new ItemStack(Material.DIAMOND_LEGGINGS);
		is.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		is.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 4);
		is.addEnchantment(Enchantment.PROTECTION_FIRE, 4);
		is.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 4);
		pi.setLeggings(is);
		is = new ItemStack(Material.DIAMOND_CHESTPLATE);
		is.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		is.addEnchantment(Enchantment.PROTECTION_EXPLOSIONS, 4);
		is.addEnchantment(Enchantment.PROTECTION_FIRE, 4);
		is.addEnchantment(Enchantment.PROTECTION_PROJECTILE, 4);
		pi.setChestplate(is);
		is = new ItemStack(Material.SPONGE);
		pi.setHelmet(is);
	}

	/**
	 * This makes a player a loser on /trophytp. It teleports them and give them
	 * the right stuff
	 * 
	 * @param p The player that is a champion
	 */
	public void loser(Player p) {
		p.teleport(trophy.getSpectatorLocation());
		//the durability of the stuff given to the players is set so that they break after 1 use.
		p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, 20 * 60 * 1, 0));
		clearInventory(p);
		// wooden sword
		PlayerInventory pi = p.getInventory();
		ItemStack is = new ItemStack(Material.WOOD_SWORD);
		is.setDurability((short) 59);
		pi.setItem(0, is);
		// bow
		is = new ItemStack(Material.BOW);
		is.setDurability((short) 384);
		pi.setItem(1, is);
		// arrow
		is = new ItemStack(Material.ARROW, 64);
		pi.setItem(2, is);
		// bad armor
		is = new ItemStack(Material.LEATHER_BOOTS);
		is.setDurability((short) 65);
		pi.setBoots(is);
		is = new ItemStack(Material.LEATHER_LEGGINGS);
		is.setDurability((short) 75);
		pi.setLeggings(is);
		is = new ItemStack(Material.LEATHER_CHESTPLATE);
		is.setDurability((short) 81);
		pi.setChestplate(is);
		is = new ItemStack(Material.DISPENSER);
		pi.setHelmet(is);
	}

	public void player(Player player, Team t, Boolean deOp) {
		if (deOp)
			setOp(player, false);
		setVanish(player);
		player.setGameMode(GameMode.SURVIVAL);

		removeFromTeams(player);
		CotmPlayer cp = getCotmPlayer(player);
		if (cp != null)
			t.addPlayer(cp);

		setSuffix(player, t.getColor(), t.getSuffix());
		player.sendMessage(OK_COLOR + "You're now a member of " + t.getColor() + t.getTeamName() + "(" + t.getSuffix() + ")");
	}

	public void removeFromTeams(String name) {
		for (Team t : teams) {
			if (t.isTeamMember(name))
				t.removePlayer(name);
		}
	}

	public void removeFromTeams(Player p) {
		for (Team t : teams) {
			if (t.isTeamMember(p))
				t.removePlayer(p.getName());
		}
	}

	public static boolean itemInHand(Material m) {
		return (m == ITEM_IN_HAND_1 || m == ITEM_IN_HAND_2 || m == ITEM_IN_HAND_3);
	}

	/**
	 * Removes the specified player's staff status and suffix, and optionally
	 * de-ops them and puts them into survival.
	 * 
	 * @param player The player to unstaff
	 * @param deOp Whether the player is also to be de-opped and put into
	 *            survival mode.
	 */
	public void unstaff(Player player, Boolean deOp) {
		setVanish(player);
		player.sendMessage(OK_COLOR + "You're unvanished!");
		if (deOp) {
			setOp(player, false);
			player.setGameMode(GameMode.SURVIVAL);
		}
		removeSuffix(player);

		player.sendMessage(OK_COLOR + "You're not a staff member anymore!");
	}

	public void setupStaffMember(Player player) {
		setOp(player, true);
		setVanish(player);

		player.setGameMode(GameMode.CREATIVE);
		PlayerInventory i = player.getInventory();
		i.setItem(0, new ItemStack(ITEM_IN_HAND_1, 1));
		i.setItem(1, new ItemStack(ITEM_IN_HAND_2, 1));
		i.setItem(2, new ItemStack(ITEM_IN_HAND_3, 1));

		i.setHelmet(new ItemStack(Material.CHAINMAIL_HELMET, 1));
		i.setChestplate(new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1));
		i.setLeggings(new ItemStack(Material.CHAINMAIL_LEGGINGS, 1));
		i.setBoots(new ItemStack(Material.CHAINMAIL_BOOTS, 1));

		removeFromTeams(player);
	}

	public void makeStreamer(Player player) {
		setSuffix(player, ChatColor.GRAY, SUFFIX_STREAMER);
		setupStaffMember(player);
		player.sendMessage(OK_COLOR + "You're now a streamer.");
	}

	public void makeRef(Player player) {
		setSuffix(player, ChatColor.DARK_GRAY, SUFFIX_REF);
		setupStaffMember(player);
		player.sendMessage(OK_COLOR + "You're now a referee.");

	}

	public void makeAssistant(Player player) {
		setSuffix(player, ChatColor.GRAY, SUFFIX_ASSISTANT);
		setupStaffMember(player);
		player.sendMessage(OK_COLOR + "You're now an assistant referee.");

	}

	public void setSuffix(Player player, ChatColor c, String suffix) {
		String name = player.getName();
		player.setDisplayName(name + " " + c + suffix + ChatColor.WHITE);

		/*
		 * This sets the color in the tab list too. if (name.length() > 14)
		 * player.setPlayerListName(c + name.substring(0, 14)); else
		 * player.setPlayerListName(c + name);
		 */
	}

	public void removeSuffix(Player player) {
		player.setDisplayName(player.getName());
	}

	public ArrayList<Player> getStaffMembers(PlayerType pt) {
		ArrayList<Player> members = new ArrayList<Player>();
		for (Player p : world.getPlayers()) {
			if (isStaffMember(p, pt)) {
				members.add(p);
			}
		}
		return members;
	}

	public ArrayList<Player> getStreamers() {
		return getStaffMembers(PlayerType.STREAMER);
	}

	public ArrayList<Player> getRefs() {
		return getStaffMembers(PlayerType.REF);
	}

	public ArrayList<Player> getAssistants() {
		return getStaffMembers(PlayerType.ASSISTANT);
	}

	public ArrayList<Player> getAllStaffMembers() {
		ArrayList<Player> members = new ArrayList<Player>();
		CotmPlayer cp;
		for (Player p : world.getPlayers()) {
			cp = getCotmPlayer(p);
			if (cp != null && cp.isStaff()) {
				members.add(p);
			}
		}
		return members;
	}

	public String getStaffMembersByName(PlayerType pt) {
		String s = "";
		for (Player p : world.getPlayers()) {
			if (isStaffMember(p, pt)) {
				s += p.getName() + ", ";
			}
		}
		if (s.length() >= 2)
			return s.substring(0, s.length() - 2);
		else
			return "";
	}

	public String getRefsByName() {
		return getStaffMembersByName(PlayerType.REF);
	}

	public String getStreamersByName() {
		return getStaffMembersByName(PlayerType.STREAMER);
	}

	public String getAssistantsByName() {
		return getStaffMembersByName(PlayerType.ASSISTANT);
	}

	public boolean isRunning() {
		return (start != null);
	}

	/**
	 * Ends the match.
	 * 
	 * @param winner The winning team
	 */
	public void gg(Team winner) {
		Server s = getServer();
		s.broadcastMessage(MAIN_COLOR + "GG");
		end = Calendar.getInstance();
		s.broadcastMessage(MAIN_COLOR + "The match took " + SIDE_COLOR + timespanHMS(start, end));
	
		cancelMinuteAlerts();
	
		champions = winner;
		String nameChampions = champions.getTeamName();
		champions.addVictorie();
		int victories = champions.getVictories();
	
		if (gameType == GameType.CHAMPIONSHIP_MATCH) {
			if (victories == 1) {
				s.broadcastMessage(MAIN_COLOR + "The new Champions are " + SIDE_COLOR + nameChampions + "!");
				s.broadcastMessage(MAIN_COLOR + nameChampions + " has won " + SIDE_COLOR + "1 championship match" + MAIN_COLOR
						+ " on this map (in a row)!");
			} else if (victories > 1) {
				s.broadcastMessage(SIDE_COLOR + nameChampions + MAIN_COLOR + " remain the Champions!");
				s.broadcastMessage(MAIN_COLOR + nameChampions + " has won " + SIDE_COLOR + victories + " championship matches" + MAIN_COLOR
						+ " on this map (in a row)!");
			}
		} else if (gameType == GameType.QUEUE_FIGHT) {
			Team losers = getOtherTeam(champions);
			losers.addLoss();
	
			s.broadcastMessage(SIDE_COLOR + nameChampions + MAIN_COLOR + " won this queue fight!");
			if (victories == 1)
				s.broadcastMessage(SIDE_COLOR + nameChampions + MAIN_COLOR + " has " + SIDE_COLOR + "1 point" + MAIN_COLOR
						+ " in the queue list!");
			else
				s.broadcastMessage(SIDE_COLOR + nameChampions + MAIN_COLOR + " has " + SIDE_COLOR + victories + " points" + MAIN_COLOR
						+ " in the queue list!");
	
			if (losers.getVictories() == 1)
				s.broadcastMessage(SIDE_COLOR + losers.getTeamName() + MAIN_COLOR + " has " + SIDE_COLOR + "1 point" + MAIN_COLOR
						+ " in the queue list!");
			else
				s.broadcastMessage(SIDE_COLOR + losers.getTeamName() + MAIN_COLOR + " has " + SIDE_COLOR + losers.getVictories() + " points"
						+ MAIN_COLOR + " in the queue list!");
		} else if (gameType == GameType.MEET_THE_MAP) {
			s.broadcastMessage(SIDE_COLOR + nameChampions + MAIN_COLOR + " won this 'meet the map' battle!");
		}
		generateStats();
		uploadUrl = uploadStats();
		announceStatsUrl(Server.BROADCAST_CHANNEL_USERS);
		if (!getDebugMode())
			announceStatsRfwbot(uploadUrl);
		notifyAdminRfwbot("GG - stats: " + uploadUrl);
	}
	
	/**
	 * Ends a scrim, displaying victory data in chat
	 * 
	 * @param winner The team which won (may be null)
	 */
	public void ggScrim(Team winner) {
		Server s = getServer();
		s.broadcastMessage(MAIN_COLOR + "GG");
		end = Calendar.getInstance();
		s.broadcastMessage(MAIN_COLOR + "The match took " + SIDE_COLOR + timespanHMS(start, end));

		cancelMinuteAlerts();

		champions = winner;
		if (champions != null) {
			String nameChampions = champions.getTeamName();
			s.broadcastMessage(SIDE_COLOR + nameChampions + MAIN_COLOR + " won this scrimmage!");
		}
		
		generateStats();
		uploadUrl = uploadStats();
		announceStatsUrl(Server.BROADCAST_CHANNEL_USERS);
		
	}
	
	/**
	 * Ends a scrim with no winner specified
	 */
	public void ggScrim() {
		ggScrim(null);
	}

	/**
	 * Ends the match, disqualifying one team and giving match victory to the other.
	 * Championship victories / queue points will not be updated
	 * 
	 * @param disqualified The team which has been disqualified
	 */
	public void ggDisqualify(Team disqualified) {
		Server s = getServer();
		s.broadcastMessage(DECISION_COLOR + disqualified.getTeamName() + " have been disqualified.");
		end = Calendar.getInstance();
		s.broadcastMessage(MAIN_COLOR + "The match took " + SIDE_COLOR + timespanHMS(start, end));
	
		cancelMinuteAlerts();
	
		champions = getOtherTeam(disqualified);
		disqualification = true;
	
		generateStats();
		uploadUrl = uploadStats();
		announceStatsUrl(Server.BROADCAST_CHANNEL_USERS);
		notifyAdminRfwbot("DQ - stats: " + uploadUrl);
		
	}
	
	public void addKill(Player p) {
		CotmPlayer cp = getCotmPlayer(p);
		if (cp != null)
			cp.addKill();
	}

	public void addDeath(Player p) {
		CotmPlayer cp = getCotmPlayer(p);
		if (cp != null)
			cp.addDeath();
	}

	public Team getTeam(int index) {
		if (index < teams.size())
			return teams.get(index);
		return null;
	}

	public Team getOtherTeam(Team t) {
		if (teams.size() >= 2) {
			if (getTeam(0) == t)
				return getTeam(1);
			return getTeam(0);
		}
		return null;
	}
	
	/**
	 * Check if the plugin is running in DEBUG mode.
	 * 
	 * @return True if the plugin is currently in DEBUG mode
	 */
	public Boolean getDebugMode() {
		return this.getConfig().getBoolean("settings.debug", true);
	}
	
	/**
	 * Set the plugin's DEBUG mode
	 * 
	 * @param debug Whether you want the server to go into debug mode
	 */
	public void setDebugMode(Boolean debug) {
		this.getConfig().set("settings.debug", debug);
	}
	
	/**
	 * Get this server's name, according to the plugin config
	 * 
	 * @return The name for this server
	 */
	public String getServerName() {
		return this.getConfig().getString("settings.servername","Unknown server");
	}
	

	/**
	 * Reset player statistics for all team members.
	 */
	public void resetPlayerStats() {
		for (CotmPlayer p : teamPlayers) {
			p.resetStats();
		}
	}

	/**
	 * Upload previously generated HTML stats to the configured upload site
	 * 
	 * @return The URL at which the uploaded file can be viewed
	 */
	private String uploadStats() {
		if (statsHtml == null)
			return null;

		try {
			URL url = new URL(STATISTICS_POST_URL);
			String postData = URLEncoder.encode("txt", "UTF-8") + "=" + URLEncoder.encode(statsHtml, "UTF-8");

			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);

			OutputStream outputStream = connection.getOutputStream();
			outputStream.write(postData.getBytes("UTF-8"));
			outputStream.close();

			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			URL statsUrl = new URL(br.readLine());
			return statsUrl.toString();

		} catch (Exception ex) {
			return null;
		}
	}
	
	/**
	 * Uses RFWbot web hooks to announce match stats 
	 * 
	 * @param statsUrl The match stats page URL
	 * @return
	 */
	private Boolean announceStatsRfwbot(String statsUrl) {
		return rfwbotSend(RFWBOT_STATS_CHANNEL,"CotM match ended! Statistics: " + statsUrl);
	}
	
	/**
	 * Sends a message via the RFWbot API 
	 * 
	 * @param channel The channel the message will be sent to
	 * @param message The message to be sent
	 * @return
	 */
	private Boolean rfwbotSend(String channel, String message) {
		try {
			URL url = new URL(RFWBOT_API_URL);
			String postData = URLEncoder.encode("key", "UTF-8") + "=" + URLEncoder.encode(RFWBOT_API_KEY, "UTF-8") + "&" +
					URLEncoder.encode("channel", "UTF-8") + "=" + URLEncoder.encode(channel, "UTF-8") + "&" +
					URLEncoder.encode("message", "UTF-8") + "=" + URLEncoder.encode(message, "UTF-8");

			URLConnection connection = url.openConnection();
			connection.setDoOutput(true);

			OutputStream outputStream = connection.getOutputStream();
			outputStream.write(postData.getBytes("UTF-8"));
			outputStream.close();

			BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			String response = br.readLine();
			return (response.equals("OK"));

		} catch (Exception ex) {
			return false;
		}
	}

	
	/**
	 * Uses RFWbot web hooks to send a message to the admins 
	 * 
	 * @param message The message to be sent
	 * @return
	 */
	private Boolean notifyAdminRfwbot(String message) {
		return rfwbotSend("cotm-plugin-admin-alert","Message from CotM plugin:\n" + message);
	}

	/**
	 * Generates and stores the HTML statistics file.
	 */
	public void generateStats() {
		statsHtml = "<html><head>\n" + "<title>" + ucfirst(getGameTitle()) + "</title>\n" + "<link rel=\"stylesheet\" type=\"text/css\" href=\""
				+ STATISTICS_STYLESHEET_URL + "\" />\n" + "</head>\n" + "<body>\n" + generateGeneralInfoHtml() + generateTeamsInfoHtml()
				+ "</body></html>";
	}

	/**
	 * Generates the general info portion of the HTML
	 * 
	 * @return the generated HTML
	 */
	public String generateGeneralInfoHtml() {

		String o = "";
		String gameinfo = "";

		gameinfo += formatTableRow("Date", +start.get(Calendar.YEAR) + "-" + (start.get(Calendar.MONTH) + 1) + "-" + start.get(Calendar.DAY_OF_MONTH));
		gameinfo += formatTableRow("Time", +start.get(Calendar.HOUR_OF_DAY) + ":" + (start.get(Calendar.MINUTE)) + " UTC");
		gameinfo += formatTableRow("Map", ucfirst(mapName));
		gameinfo += formatTableRow("Type of game", ucfirst(gameType.getName()));

		o += "<div class=\"cotm-titles\">\n";

		// Write map title
		o += "<h1 class=\"cotm-gametitle\">" + ucfirst(getGameTitle()) + "</h1>\n";

		// Write teams
		if (teams.size() == 2) {
			Team t1 = getTeam(0);
			Team t2 = getTeam(1);
			Team losers = getOtherTeam(champions);
			if (gameType == GameType.CHAMPIONSHIP_MATCH) {
				Team challengers = getChallengers();
				Team defenders = getOtherTeam(t1);
				if (challengers == null || defenders == null) {
					o += "<h2 class=\"cotm-teamnames\">" + t1.getTeamName() + " vs. " + t2.getTeamName() + "</h2>\n";
				} else if (challengers.getOldVictories() == 0 && defenders.getOldVictories() == 0) {
					o += "<h2 class=\"cotm-teamnames\">" + t1.getTeamName() + " vs. " + t2.getTeamName() + "</h2>\n";
				} else {
					o += "<h2 class=\"cotm-teamnames\">" + challengers.getTeamName() + " challenges " + defenders.getTeamName() + "</h2>\n";
				}

				if (!disqualification) {
					gameinfo += formatTableRow("Champions", champions.getTeamName());
					gameinfo += formatTableRow("Champions for", champions.getVictories() + " games");
				} else {
					gameinfo += formatTableRow("Outcome",losers.getTeamName() + " disqualified");
				}

			} else {
				o += "<h2 class=\"cotm-teamnames\">" + t1.getTeamName() + " vs. " + t2.getTeamName() + "</h2>\n";

				if (disqualification) {
					gameinfo += formatTableRow("Outcome",losers.getTeamName() + " disqualified");
				} else if (gameType == GameType.QUEUE_FIGHT) {
					gameinfo += formatTableRow("Winners", champions.getTeamName());
					if (champions.getVictories() == 1) {
						gameinfo += formatTableRow("Points of " + champions.getTeamName(), "1 point");
					} else {
						gameinfo += formatTableRow("Points of " + champions.getTeamName(), champions.getVictories() + " points");
					}
					if (losers.getVictories() == 1) {
						gameinfo += formatTableRow("Points of " + losers.getTeamName(), "1 point");
					} else {
						gameinfo += formatTableRow("Points of " + losers.getTeamName(), losers.getVictories() + " points");
					}
				} else {
					if (champions != null)
						gameinfo += formatTableRow("Winners", champions.getTeamName());
				}
			}
		}

		o += "</div>\n";

		o += "<div class=\"cotm-section-general\">\n";

		gameinfo += formatTableRow("Duration", timespanHMS(start, end));
		gameinfo += formatTableRow("Refs", getRefsByName());
		gameinfo += formatTableRow("Assistant refs", getAssistantsByName());
		gameinfo += formatTableRow("Streamers", getStreamersByName());
		gameinfo += formatTableRow("Server", getServerName());
		// gameinfo +=
		// formatTableRow("Videos","<span class='italic'>None available yet</span>");
		// gameinfo +=
		// formatTableRow("Images","<span class='italic'>None available yet</span>");

		// Write game information
		o += "<table class=\"cotm-gameinfo\">\n";

		o += "<thead><tr><th colspan=2>Game Information</th></tr></thead>\n";
		o += "<tbody>" + gameinfo + "</tbody>\n";
		o += "</table>\n";
		o += "</div>\n";

		return o;
	}

	/**
	 * Generates the teams info section of the HTML stats file
	 * 
	 * @return the generated HTML
	 */
	public String generateTeamsInfoHtml() {
		String o = "";
		if (teams.size() > 0) {
			o += "<div class=\"cotm-section-teams\">\n";

			for (Team t : teams) {
				o += generateTeamStatsHtml(t);
			}
			o += "</div>\n";
		}
		return o;
	}

	/**
	 * Generates the team data HTML table for the specified team
	 * 
	 * @param t the team to generate html for
	 * @return the generated HTML
	 */
	public String generateTeamStatsHtml(Team t) {
		String o = "";
		o += "<table class=\"cotm-teaminfo\">\n";
		o += "<thead><tr><th colspan=3>" + t.getTeamName() + "</th></tr>\n";
		o += "<tr><th>Name</th><th>Deaths</th><th>Kills</th></tr></thead>\n";
		o += "<tbody>\n";

		for (CotmPlayer cp : t.getTeamMembers()) {
			o += "<tr><td class=\"cotm-playername\" style=\"background:url('https://minotar.net/avatar/" + cp.getName()
					+ "/32') no-repeat 0.5em center;\">" + cp.getName() + "</td><td>" + cp.getDeaths() + "</td><td>" + cp.getKills() + "</td></tr>\n";
		}
		o += "</tbody></table>\n";

		return o;
	}

	/**
	 * Convenience function to format an HTML &lt;tr&gt; containing two td
	 * elements
	 * 
	 * @param key The contents of the first table cell
	 * @param value The contents of the second table cell
	 * @return The generated HTML
	 */
	private String formatTableRow(String key, String value) {
		return "<tr><td>" + key + "</td><td>" + value + "</td></tr>\n";
	}

	/**
	 * Open an HTTP connection to a URL and get an InputStream to read its
	 * content.
	 * 
	 * @param urlToGet The URL to be opened
	 * @return The stream to read data from
	 * @throws IOException
	 */
	private InputStream getUrlInputStream(String urlToGet) throws IOException {
		URL url = new URL(urlToGet);
		return url.openConnection().getInputStream();
	}

	/**
	 * Loads YAML data from a URL
	 * 
	 * @param url The URL to load data from
	 * @return The configuration data
	 * @throws IOException
	 * @throws InvalidConfigurationException
	 */
	private YamlConfiguration getMatchConfig(String url) throws IOException, InvalidConfigurationException {
		YamlConfiguration cfg = new YamlConfiguration();
		InputStream stream = getUrlInputStream(url);
		if (stream == null)
			return null;

		cfg.load(stream);
		stream.close();
		return cfg;
	}

	public GameType getGameType() {
		return gameType;
	}

	public void setGameType(GameType g) {
		gameType = g;
	}

	public void setMapName(String m) {
		mapName = m;
	}

	public void setGameTitle(String g) {
		gameTitle = g;
	}

	public String getGameTitle() {
		if (gameTitle != null)
			return gameTitle;

		if (mapName != null && !mapName.isEmpty()) {
			if (gameType == GameType.QUEUE_FIGHT) {
				return "a Champions of " + mapName + " queue fight";
			} else if (gameType == GameType.CHAMPIONSHIP_MATCH) {
				return "Champions of " + mapName;
			} else if (gameType == GameType.MEET_THE_MAP) {
				return "Meet the Map: " + mapName;
			} else if (gameType == GameType.SCRIMMAGE) {
				return "a scrimmage on " + mapName;
			}
		} else {
			if (gameType == GameType.QUEUE_FIGHT) {
				return "a Champions of the Map queue fight";
			} else if (gameType == GameType.CHAMPIONSHIP_MATCH) {
				return "Champions of the Map";
			} else if (gameType == GameType.MEET_THE_MAP) {
				return "Meet the Map";
			} else if (gameType == GameType.SCRIMMAGE) {
				return "a scrimmage";
			}
		}
		return "a scrimmage";
	}

	public void openInventoryPlayer(Player staff, Player p) {
		PlayerInventory i = (PlayerInventory) p.getInventory();

		Inventory i2 = Bukkit.createInventory(staff, 45, ChatColor.DARK_GRAY + p.getDisplayName());
		for (int j = 0; j < 36; j++) {
			if (j < 36) {
				if (j < 9)
					i2.setItem(j + 27, i.getItem(j));
				else
					i2.setItem(j - 9, i.getItem(j));
			}
		}
		i2.setItem(36, i.getHelmet());
		i2.setItem(37, i.getChestplate());
		i2.setItem(38, i.getLeggings());
		i2.setItem(39, i.getBoots());
		i2.setItem(40, null);
		i2.setItem(41, null);
		i2.setItem(42, null);
		i2.setItem(43, new ItemStack(HEALTH_ITEM, p.getHealth()));
		i2.setItem(44, new ItemStack(FOOD_ITEM, p.getFoodLevel()));

		staff.openInventory(i2);
	}

	public World getWorld() {
		return world;
	}

	public void loadMapData() {
		ArrayList<String> lines = new ArrayList<String>();
		try {
			worldFolder = this.getDataFolder().getAbsoluteFile().getParentFile().getParentFile().getPath() + File.separator + world.getName();
			fWarps = new File(worldFolder + File.separator + MAP_DATA_FILE);
			if (fWarps.exists() == false) {
				fWarps.createNewFile();
			}

			FileReader fr = new FileReader(fWarps);
			BufferedReader in = new BufferedReader(fr);
			String s = in.readLine();

			while (s != null) {
				lines.add(s);
				s = in.readLine();
			}

			in.close();
			fr.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		if (lines.size() >= 2) {
			if (lines.get(0).equals(MAP_DATA_FORMAT_STRING)) {
				setMapName(lines.get(1));
				if (!mapName.isEmpty()) {
					getServer().broadcast(OK_COLOR + "Loading CotM warps for " + mapName, Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
				}
				for (int i = 2; i < lines.size(); i++) {
					try {
						CotmWarp warp = new CotmWarp(lines.get(i), world);
						if (warp.isCyclable())
							warpCycle.add(warp);
						warps.add(warp);
					} catch (NumberFormatException e) {
						getServer().broadcast(ERROR_COLOR + "The warp on line " + (i + 1) + " of " + MAP_DATA_FILE + " couldn't be loaded.",
								Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
					}
				}
			} else {
				getServer().broadcast(ERROR_COLOR + "Format of " + MAP_DATA_FILE + " doesn't match", Server.BROADCAST_CHANNEL_ADMINISTRATIVE);
			}
		}
	}

	public void saveMapData() {
		if (world != null) {
			try {
				worldFolder = this.getDataFolder().getAbsoluteFile().getParentFile().getParentFile().getPath() + File.separator + world.getName();
				fWarps = new File(worldFolder + File.separator + MAP_DATA_FILE);
				if (fWarps.exists() == false) {
					fWarps.createNewFile();
				}

				FileWriter fw = new FileWriter(fWarps);
				BufferedWriter out = new BufferedWriter(fw);

				out.write(MAP_DATA_FORMAT_STRING);
				out.newLine();

				if (mapName != null)
					out.write(mapName);
				else
					out.write("");

				out.newLine();

				for (CotmWarp cw : warps) {
					out.write(cw.toFormattedString());
					out.newLine();
				}

				out.close();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void setLastDeathLocation(Location l, String name) {
		lastDeathLocation = l;
		lastDeathPlayerName = name;
		lastEventLocation = l;
	}

	public void setLastNotifierLocation(Location l) {
		lastNotifierLocation = l;
		lastEventLocation = l;
	}

	public void setLastLogoutLocation(Location l) {
		lastLogoutLocation = l;
	}

	public String ucfirst(String input) {
		if (input == null || input.isEmpty())
			return input;
		return Character.toUpperCase(input.charAt(0)) + input.substring(1);
	}

	private String setTeam(Player sender, String suffix, String name, String color, int victories, List<String> players) {
		String output = "";
		String args[] = { "", "" };
		// only set the team if there's a suffix
		if (suffix != null) {
			// Set the suffix and the team name
			args[0] = suffix;
			if (name != null) {
				args[1] = name;
			}
			cAddteam(args);

			Team t = getTeamBySuffix(suffix);

			// set the color
			int colorIndex = getColorIndex(color);
			if (color != null && colorIndex != -1) {
				ChatColor chatColor = ChatColor.getByChar(COLOR_DATA[colorIndex]);
				t.setColor(chatColor, getServer());
			}

			// set the victories
			t.setVictories(victories);

			output += OK_COLOR + "Created team " + t.toString() + "\n";

			// add the players.

			if (players.size() > 0) {
				String s = suffix + " ";
				for (String playerName : players) {
					s += playerName + " ";
				}
				if (s.length() > 1)
					s.substring(0, s.length() - 1);
				String args1[] = s.split(" ");
				output += cPlayer(sender, args1);
			}

			output += t.getInfo() + "\n";
			return output;
		} else
			return ERROR_COLOR + "There was no suffix specified. Couldn't load the team." + "\n";
	}

	public int getColorIndex(String colorName) {
		for (int i = 0; i < COLOR_NAMES.length; i++) {
			if (COLOR_NAMES[i].equalsIgnoreCase(colorName)) {
				return i;
			}
		}
		return -1;
	}
}
