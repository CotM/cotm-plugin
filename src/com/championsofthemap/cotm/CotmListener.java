package com.championsofthemap.cotm;

import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerExpChangeEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;

public class CotmListener implements Listener {

	private Cotm c;

	public CotmListener(Cotm c) {
		this.c = c;
	}
	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e) {
		if (e.getMessage().toLowerCase().startsWith("/toggledownfall")) {
			if (c.isRunning() && !c.isRef(e.getPlayer())) {
				e.setCancelled(true);
				e.getPlayer().sendMessage(Cotm.ERROR_COLOR + "That command can only be executed by referees");
			}
		}
	}

	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		if (c.isRunning()) {
			Player p = e.getEntity();
			c.addDeath(p);
			if (p.getKiller() != null)
				c.addKill(p.getKiller());
		}
		Player dead = e.getEntity();
		String s = e.getDeathMessage();
		int i = s.indexOf(dead.getName());
		s = s.substring(0, i) + dead.getDisplayName() + s.substring(i + dead.getName().length());
		if (dead.getKiller() != null) {
			Player killer = dead.getKiller();
			i = s.indexOf(killer.getName(), i + dead.getDisplayName().length());
			if (i != -1)
				s = s.substring(0, i) + killer.getDisplayName() +s.substring(i + killer.getName().length());
		}
		e.setDeathMessage(s);
		if (e.getDeathMessage().indexOf("fell out of the world") == -1)
			c.setLastDeathLocation(e.getEntity().getLocation(), e.getEntity().getDisplayName());
	}

	/**
	 * Handle right-clicking interactions
	 * 
	 * @param e
	 */
	@EventHandler
	public void onInteractWithEntity(PlayerInteractEntityEvent e) {
		// right-clicking on a player displays inventory
		if (c.isStaff(e.getPlayer()) && e.getRightClicked() instanceof Player) {
			Player staff = e.getPlayer();
			Player p = (Player) e.getRightClicked();
			c.openInventoryPlayer(staff, p);
			e.setCancelled(true);
		}
		// Cancel all other interactions if special item is in hand, except
		// minecart chest
		/*
		 * if (e.getPlayer().getItemInHand().getType() == Cotm.ITEM_IN_HAND &&
		 * c.isStaff((Player) e.getPlayer())) { if (!(e.getRightClicked()
		 * instanceof StorageMinecart)) { e.setCancelled(true); } }
		 */

	}

	/**
	 * Handle right-clicking of blocks
	 * 
	 * Right-click chest with special item to open silently
	 * 
	 * @param e
	 */
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if (Cotm.itemInHand(e.getMaterial()) && c.isStaff(e.getPlayer())) {
			// Handle interactions between staff and blocks when special item is
			// held
			if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
				BlockState blockState = e.getClickedBlock().getState();
				if (blockState instanceof Chest) {
					// Right-clicking chest displays contents (silent chest)
					Chest c = (Chest) e.getClickedBlock().getState();
					Player staff = ((Player) e.getPlayer());
					Inventory i = c.getInventory();
					Inventory i2 = null;
					if (i.getSize() == 27) {
						i2 = Bukkit.createInventory(staff, 36, "CotM Silent Chest");
						for (int j = 0; j < 36; j++) {
							if (j < 27)
								i2.setItem(j, i.getItem(j));
							else
								i2.setItem(j, null);
						}
					} else if (i.getSize() == 54) {
						i2 = Bukkit.createInventory(staff, 63, "CotM Silent Chest");
						for (int j = 0; j < 63; j++) {
							if (j < 54)
								i2.setItem(j, i.getItem(j));
							else
								i2.setItem(j, null);
						}
					}
					staff.openInventory(i2);

				}
				// Prevent interaction with any other blocks, except dispensers
				// (23), furnaces (61/62), levers (69), buttons (77) and brewing stands (117)
				int typeId = e.getClickedBlock().getTypeId();
				if (typeId != 23 && typeId != 61 && typeId != 62 && typeId != 69 && typeId != 77 && typeId != 117) {
					e.setUseInteractedBlock(Result.DENY);
					e.setCancelled(true);
				}
			} else if (e.getAction() == Action.LEFT_CLICK_BLOCK && e.getMaterial() == Cotm.ITEM_IN_HAND_1) {
				// Left-clicking a block = pass-through

				Location l = e.getClickedBlock().getLocation();
				Player p = e.getPlayer();
				Location pl = p.getLocation();
				l.setPitch(pl.getPitch());
				l.setYaw(pl.getYaw());
				int dx = 0, dz = 0, dy = 0;
				BlockFace bf = e.getBlockFace();
				if (bf == BlockFace.NORTH) {
					dx = 1;
				} else if (bf == BlockFace.EAST) {
					dz = 1;
				} else if (bf == BlockFace.SOUTH) {
					dx = -1;
				} else if (bf == BlockFace.WEST) {
					dz = -1;
				} else if (bf == BlockFace.UP) {
					dy = -1;
				} else if (bf == BlockFace.DOWN) {
					dy = 1;
				}
				int i = 1;
				World w = l.getWorld();
				int x = l.getBlockX(), y = l.getBlockY(), z = l.getBlockZ();
				do {
					x = x + dx;
					y = y + dy;
					z = z + dz;
					i++;
				} while (i < 25 && (!(Cotm.isSpaceForPlayer(w, x, y, z))));

				// teleport if there is a free spot.
				if (i != 25 && y >= 0) {
					l.setX(x);
					l.setY(y);
					l.setZ(z);
					p.teleport(l); // note: this teleport does not save
									// location for /tpback
					if (p.getGameMode() == GameMode.CREATIVE)
						p.setFlying(true);
					p.sendMessage(Cotm.OK_COLOR + "You passed through the wall!");
				} else {
					p.sendMessage(Cotm.ERROR_COLOR + "Nothing to pass through!");
				}

				e.setCancelled(true);
			} else if (e.getMaterial() == Cotm.ITEM_IN_HAND_2 && (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK)) {
				c.nextPlayer(e.getPlayer());
				e.setCancelled(true);
			} else if (e.getMaterial() == Cotm.ITEM_IN_HAND_3 && (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK)) {
				c.nextWarp(e.getPlayer());
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		CotmPlayer cp = c.getCotmPlayer(e.getPlayer());
		if (cp != null) {
			cp.setStatus();
		} else
			c.setVanish(e.getPlayer());
		e.setJoinMessage(ChatColor.YELLOW + e.getPlayer().getDisplayName() + ChatColor.YELLOW + " joined the game.");
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		e.setQuitMessage(ChatColor.YELLOW + e.getPlayer().getDisplayName() + ChatColor.YELLOW + " left the game.");

		// TODO remove this when bukkit fixes this.
		c.unvanish(e.getPlayer());
		
		c.setLastLogoutLocation(e.getPlayer().getLocation());
	}

	@EventHandler
	public void onLogin(PlayerLoginEvent e) {
		if (e.getResult() == PlayerLoginEvent.Result.KICK_WHITELIST) {
			String name = e.getPlayer().getName();
			if (c.getCotmPlayer(name) != null) {
				e.setResult(PlayerLoginEvent.Result.ALLOWED);
			}
		}
	}

	@EventHandler(ignoreCancelled = true)
	public void onPlayerPickupItem(PlayerPickupItemEvent event) {
		if (c.isStaff(event.getPlayer()))
			event.setCancelled(true);
	}

	@EventHandler(ignoreCancelled = true)
	public void onPlayerDropItem(PlayerDropItemEvent event) {
		if (c.isStaff(event.getPlayer()))
			event.setCancelled(true);
	}

	@EventHandler(ignoreCancelled = true)
	public void onEntityTarget(EntityTargetEvent event) {
		if (!(event.getTarget() instanceof Player))
			return;

		Player player = (Player) event.getTarget();
		if (c.isStaff(player))
			event.setCancelled(true);
	}

	@EventHandler(ignoreCancelled = true)
	public void onEntityDamage(EntityDamageEvent event) {
		if (event instanceof EntityDamageByEntityEvent) {
			EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event;
			if (e.getDamager() instanceof Player) {
				Player damager = (Player) e.getDamager();
				if (c.isStaff(damager)) {
					event.setCancelled(true);
					if (damager.getItemInHand().getType() == Cotm.ITEM_IN_HAND_2) {
						c.nextPlayer(damager);
					} else if (damager.getItemInHand().getType() == Cotm.ITEM_IN_HAND_3) {
						c.nextWarp(damager);
					}
				}
			}
		}
		
		if (event.getEntity() instanceof Player) {
			Player victim = (Player) event.getEntity();
			if (c.isStaff(victim)) {
				event.setCancelled(true);
			}
		}
	}

	/**
	 * Prevent staff from gaining xp (they will probably still pick up orbs)
	 *
	 * @param event The PlayerExpChangeEvent to be handled
	 */
	@EventHandler
	public void onPlayerExpChangeEvent(PlayerExpChangeEvent event) {
		if (c.isStaff(event.getPlayer()))
			event.setAmount(0);
	}
	
	
	/**
	 * Handle chat messages, prevent normal referee chat from being seen by players
	 * 
	 * @param event
	 */
	@EventHandler(priority=EventPriority.HIGHEST)
	public void chatMessage(AsyncPlayerChatEvent event) {
		// Pre- or post-match, don't do anything
		if (!c.matchRunning()) return;
		
		// If it's a scrim, don't do anything
		if (c.getGameType()==GameType.SCRIMMAGE) return;
		
		Player player = event.getPlayer();

		// Try to get a CotmPlayer for player
		CotmPlayer cp=c.getCotmPlayer(player);

		// if this is an unknown player on the server, don't do anything
		if (cp == null) return;
		
		if (cp.isStaff()) {
			// If staff, we will normally only send the message to other staff
			
			// If message begins with !, strip off the ! and broadcast it regardless
			if (event.getMessage().startsWith("!")) {
				event.setMessage(event.getMessage().substring(1));
				return;
			}
			
			// Otherwise, prefix the message with [S] to indicate private chat
			event.setMessage("[S] " + event.getMessage());
		
			Iterator<Player> iter = event.getRecipients().iterator();
			while (iter.hasNext()) {
				Player recipient = iter.next();
	
				// Get the CotmPlayer for the recipient
				CotmPlayer cpRecipient = c.getCotmPlayer(recipient);
				if (cpRecipient != null) {
					// Don't send the message if the other player isn't staff
					if (!cpRecipient.isStaff()) {
						iter.remove();
						continue;
					}
				}
			}
		}
	}
	
}
