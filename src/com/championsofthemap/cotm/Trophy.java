package com.championsofthemap.cotm;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Biome;

public class Trophy {

	private static final int[][][] NUMBERS = {
			{ { 0, 1, 1, 1, 0 }, { 1, 0, 0, 0, 1 }, { 1, 0, 0, 1, 1 }, { 1, 0, 1, 0, 1 }, { 1, 1, 0, 0, 1 }, { 1, 0, 0, 0, 1 }, { 0, 1, 1, 1, 0 } },
			{ { 0, 0, 1, 0, 0 }, { 0, 1, 1, 0, 0 }, { 0, 0, 1, 0, 0 }, { 0, 0, 1, 0, 0 }, { 0, 0, 1, 0, 0 }, { 0, 0, 1, 0, 0 }, { 1, 1, 1, 1, 1 } },
			{ { 0, 1, 1, 1, 0 }, { 1, 0, 0, 0, 1 }, { 0, 0, 0, 0, 1 }, { 0, 0, 1, 1, 0 }, { 0, 1, 0, 0, 0 }, { 1, 0, 0, 0, 1 }, { 1, 1, 1, 1, 1 } },
			{ { 0, 1, 1, 1, 0 }, { 1, 0, 0, 0, 1 }, { 0, 0, 0, 0, 1 }, { 0, 1, 1, 1, 0 }, { 0, 0, 0, 0, 1 }, { 1, 0, 0, 0, 1 }, { 0, 1, 1, 1, 0 } },
			{ { 0, 0, 0, 1, 1 }, { 0, 0, 1, 0, 1 }, { 0, 1, 0, 0, 1 }, { 1, 0, 0, 0, 1 }, { 1, 1, 1, 1, 1 }, { 0, 0, 0, 0, 1 }, { 0, 0, 0, 0, 1 } },
			{ { 1, 1, 1, 1, 1 }, { 1, 0, 0, 0, 0 }, { 1, 1, 1, 1, 0 }, { 0, 0, 0, 0, 1 }, { 0, 0, 0, 0, 1 }, { 1, 0, 0, 0, 1 }, { 0, 1, 1, 1, 0 } },
			{ { 0, 0, 1, 1, 0 }, { 0, 1, 0, 0, 0 }, { 1, 0, 0, 0, 0 }, { 1, 1, 1, 1, 0 }, { 1, 0, 0, 0, 1 }, { 1, 0, 0, 0, 1 }, { 0, 1, 1, 1, 0 } },
			{ { 1, 1, 1, 1, 1 }, { 1, 0, 0, 0, 1 }, { 0, 0, 0, 0, 1 }, { 0, 0, 0, 1, 0 }, { 0, 0, 1, 0, 0 }, { 0, 0, 1, 0, 0 }, { 0, 0, 1, 0, 0 } },
			{ { 0, 1, 1, 1, 0 }, { 1, 0, 0, 0, 1 }, { 1, 0, 0, 0, 1 }, { 0, 1, 1, 1, 0 }, { 1, 0, 0, 0, 1 }, { 1, 0, 0, 0, 1 }, { 0, 1, 1, 1, 0 } },
			{ { 0, 1, 1, 1, 0 }, { 1, 0, 0, 0, 1 }, { 1, 0, 0, 0, 1 }, { 0, 1, 1, 1, 1 }, { 0, 0, 0, 0, 1 }, { 0, 0, 0, 1, 0 }, { 0, 1, 1, 0, 0 } } };
	// private static final int[][] NUMBER_0 =
	// {{0,1,1,1,0},{1,0,0,0,1},{1,0,0,1,1},{1,0,1,0,1},{1,1,0,0,1},{1,0,0,0,1},{0,1,1,1,0}};
	// private static final int[][] NUMBER_1 =
	// {{0,0,1,0,0},{0,1,1,0,0},{0,0,1,0,0},{0,0,1,0,0},{0,0,1,0,0},{0,0,1,0,0},{1,1,1,1,1}};
	// private static final int[][] NUMBER_2 =
	// {{0,1,1,1,0},{1,0,0,0,1},{0,0,0,0,1},{0,0,1,1,0},{0,1,0,0,0},{1,0,0,0,1},{1,1,1,1,1}};
	// private static final int[][] NUMBER_3 =
	// {{0,1,1,1,0},{1,0,0,0,1},{0,0,0,0,1},{0,1,1,1,0},{0,0,0,0,1},{1,0,0,0,1},{0,1,1,1,0}};
	// private static final int[][] NUMBER_4 =
	// {{0,0,0,1,1},{0,0,1,0,1},{0,1,0,0,1},{1,0,0,0,1},{1,1,1,1,1},{0,0,0,0,1},{0,0,0,0,1}};
	// private static final int[][] NUMBER_5 =
	// {{1,1,1,1,1},{1,0,0,0,0},{1,1,1,1,0},{0,0,0,0,1},{0,0,0,0,1},{1,0,0,0,1},{0,1,1,1,0}};
	// private static final int[][] NUMBER_6 =
	// {{0,0,1,1,0},{0,1,0,0,0},{1,0,0,0,0},{1,1,1,1,0},{1,0,0,0,1},{1,0,0,0,1},{0,1,1,1,0}};
	// private static final int[][] NUMBER_7 =
	// {{1,1,1,1,1},{1,0,0,0,1},{0,0,0,0,1},{0,0,0,1,0},{0,0,1,0,0},{0,0,1,0,0},{0,0,1,0,0}};
	// private static final int[][] NUMBER_8 =
	// {{0,1,1,1,0},{1,0,0,0,1},{1,0,0,0,1},{0,1,1,1,0},{1,0,0,0,1},{1,0,0,0,1},{0,1,1,1,0}};
	// private static final int[][] NUMBER_9 =
	// {{0,1,1,1,0},{1,0,0,0,1},{1,0,0,0,1},{0,1,1,1,1},{0,0,0,0,1},{0,0,0,1,0},{0,1,1,0,0}};

	private static final Material[] MATS = { Material.ICE, Material.NETHER_BRICK, Material.IRON_BLOCK, Material.GOLD_BLOCK, Material.DIAMOND_BLOCK };
	private static final Material[] CMATS = { Material.ICE, Material.NETHER_BRICK, Material.IRON_BLOCK, Material.GOLD_BLOCK, Material.GLASS };

	private static final int BLOCKS_ABOVE_SPAWN = 5;

	private int x, y, z;
	private Material mat;
	private Material cMat; // Material for the c
	private World w;
	private int victories;

	public Trophy(World w, int victories) {
		Location loc = w.getSpawnLocation();
		this.x = loc.getBlockX() - 4;
		this.z = loc.getBlockZ() - 4;
		this.y = w.getHighestBlockYAt(x, z) + BLOCKS_ABOVE_SPAWN;
		if (victories < 1)
			victories = 1;
		if (victories <= 5) {
			this.mat = MATS[victories - 1];
			this.cMat = mat;
		} else if (victories <= 10) {
			this.mat = MATS[4];
			this.cMat = CMATS[victories - 6];
		} else {
			this.mat = MATS[4];
			this.cMat = CMATS[4];
		}
		this.victories = victories;
		this.w = w;
	}
	
	public void reset(World w, int victories){
		if (victories <= 5) {
			this.mat = MATS[victories - 1];
			this.cMat = mat;
		} else if (victories <= 10) {
			this.mat = MATS[4];
			this.cMat = CMATS[victories - 6];
		} else {
			this.mat = MATS[4];
			this.cMat = CMATS[4];
		}
		this.victories = victories;
		this.w = w;
		
		clearNumbers();
	}
	
	public void clearNumbers(){
		Material air = Material.AIR;

		for (int i = 0; i <= 4; i++) {
			for (int j = 0; j <= 6; j++) {
				w.getBlockAt(x + 2 - i, y + 27 - j, z).setType(air);
				w.getBlockAt(x + 10 - i, y + 27 - j, z).setType(air);
			}
		}
	}

	public void build() {
		// bottom 2 layers
		for (int i = 1; i <= 6; i++) {
			for (int j = 1; j <= 6; j++) {
				w.getBlockAt(x + i, y, z + j).setType(mat);
				w.getBlockAt(x + i, y + 1, z + j).setType(mat);
			}
		}

		// 3rd and 6th layer
		w.getBlockAt(x + 2, y + 2, z + 3).setType(mat);
		w.getBlockAt(x + 2, y + 2, z + 4).setType(mat);
		w.getBlockAt(x + 3, y + 2, z + 3).setType(mat);
		w.getBlockAt(x + 3, y + 2, z + 4).setType(mat);
		w.getBlockAt(x + 4, y + 2, z + 3).setType(mat);
		w.getBlockAt(x + 4, y + 2, z + 4).setType(mat);
		w.getBlockAt(x + 5, y + 2, z + 3).setType(mat);
		w.getBlockAt(x + 5, y + 2, z + 4).setType(mat);

		w.getBlockAt(x + 2, y + 5, z + 3).setType(mat);
		w.getBlockAt(x + 2, y + 5, z + 4).setType(mat);
		w.getBlockAt(x + 3, y + 5, z + 3).setType(mat);
		w.getBlockAt(x + 3, y + 5, z + 4).setType(mat);
		w.getBlockAt(x + 4, y + 5, z + 3).setType(mat);
		w.getBlockAt(x + 4, y + 5, z + 4).setType(mat);
		w.getBlockAt(x + 5, y + 5, z + 3).setType(mat);
		w.getBlockAt(x + 5, y + 5, z + 4).setType(mat);

		// 4th and 5th layer
		w.getBlockAt(x + 3, y + 3, z + 3).setType(mat);
		w.getBlockAt(x + 3, y + 3, z + 4).setType(mat);
		w.getBlockAt(x + 4, y + 3, z + 3).setType(mat);
		w.getBlockAt(x + 4, y + 3, z + 4).setType(mat);
		w.getBlockAt(x + 3, y + 4, z + 3).setType(mat);
		w.getBlockAt(x + 3, y + 4, z + 4).setType(mat);
		w.getBlockAt(x + 4, y + 4, z + 3).setType(mat);
		w.getBlockAt(x + 4, y + 4, z + 4).setType(mat);

		// 7th layer
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				w.getBlockAt(x + i, y + 6, z + j).setType(mat);
			}
		}
		// destroy the 4 lower corners
		Material air = Material.AIR;
		w.getBlockAt(x, y + 6, z).setType(air);
		w.getBlockAt(x + 7, y + 6, z).setType(air);
		w.getBlockAt(x + 7, y + 6, z + 7).setType(air);
		w.getBlockAt(x, y + 6, z + 7).setType(air);

		// 8th to 14th layer (the cup)
		for (int i = 0; i <= 7; i++) {
			for (int j = 7; j <= 13; j++) {
				w.getBlockAt(x + i, y + j, z).setType(mat);
				w.getBlockAt(x + i, y + j, z + 7).setType(mat);
				w.getBlockAt(x, y + j, z + i).setType(mat);
				w.getBlockAt(x + 7, y + j, z + i).setType(mat);
			}
		}

		// The C for champions
		w.getBlockAt(x + 2, y + 8, z).setType(cMat);
		w.getBlockAt(x + 3, y + 8, z).setType(cMat);
		w.getBlockAt(x + 4, y + 8, z).setType(cMat);
		w.getBlockAt(x + 5, y + 8, z).setType(cMat);
		w.getBlockAt(x + 5, y + 9, z).setType(cMat);
		w.getBlockAt(x + 5, y + 10, z).setType(cMat);
		w.getBlockAt(x + 5, y + 11, z).setType(cMat);
		w.getBlockAt(x + 4, y + 11, z).setType(cMat);
		w.getBlockAt(x + 3, y + 11, z).setType(cMat);
		w.getBlockAt(x + 2, y + 11, z).setType(cMat);

		// right handle
		w.getBlockAt(x - 1, y + 8, z + 3).setType(mat);
		w.getBlockAt(x - 1, y + 8, z + 4).setType(mat);
		w.getBlockAt(x - 2, y + 8, z + 3).setType(mat);
		w.getBlockAt(x - 2, y + 8, z + 4).setType(mat);
		w.getBlockAt(x - 3, y + 8, z + 3).setType(mat);
		w.getBlockAt(x - 3, y + 8, z + 4).setType(mat);
		w.getBlockAt(x - 3, y + 9, z + 3).setType(mat);
		w.getBlockAt(x - 3, y + 9, z + 4).setType(mat);
		w.getBlockAt(x - 3, y + 10, z + 3).setType(mat);
		w.getBlockAt(x - 3, y + 10, z + 4).setType(mat);
		w.getBlockAt(x - 3, y + 11, z + 3).setType(mat);
		w.getBlockAt(x - 3, y + 11, z + 4).setType(mat);
		w.getBlockAt(x - 1, y + 11, z + 3).setType(mat);
		w.getBlockAt(x - 1, y + 11, z + 4).setType(mat);
		w.getBlockAt(x - 2, y + 11, z + 3).setType(mat);
		w.getBlockAt(x - 2, y + 11, z + 4).setType(mat);

		// left handle
		w.getBlockAt(x + 8, y + 8, z + 3).setType(mat);
		w.getBlockAt(x + 8, y + 8, z + 4).setType(mat);
		w.getBlockAt(x + 9, y + 8, z + 3).setType(mat);
		w.getBlockAt(x + 9, y + 8, z + 4).setType(mat);
		w.getBlockAt(x + 10, y + 8, z + 3).setType(mat);
		w.getBlockAt(x + 10, y + 8, z + 4).setType(mat);
		w.getBlockAt(x + 10, y + 9, z + 3).setType(mat);
		w.getBlockAt(x + 10, y + 9, z + 4).setType(mat);
		w.getBlockAt(x + 10, y + 10, z + 3).setType(mat);
		w.getBlockAt(x + 10, y + 10, z + 4).setType(mat);
		w.getBlockAt(x + 10, y + 11, z + 3).setType(mat);
		w.getBlockAt(x + 10, y + 11, z + 4).setType(mat);
		w.getBlockAt(x + 9, y + 11, z + 3).setType(mat);
		w.getBlockAt(x + 9, y + 11, z + 4).setType(mat);
		w.getBlockAt(x + 8, y + 11, z + 3).setType(mat);
		w.getBlockAt(x + 8, y + 11, z + 4).setType(mat);

		// the water
		Material water = Material.WATER;
		for (int i = 1; i <= 6; i++) {
			for (int j = 1; j <= 6; j++) {
				w.setBiome(x + i, z + j, Biome.DESERT);
				for (int k = 7; k <= 13; k++) {
					w.getBlockAt(x + i, y + k, z + j).setType(water);
				}
			}
		}

		// where the losing team are teleported
		for (int i = 2; i <= 5; i++) {
			for (int j = 31; j <= 34; j++) {
				w.getBlockAt(x + i, y, z - j).setType(mat);
			}
		}
		w.getBlockAt(x + 1, y + 1, z - 30).setType(mat);
		w.getBlockAt(x + 2, y + 1, z - 30).setType(mat);
		w.getBlockAt(x + 3, y + 1, z - 30).setType(mat);
		w.getBlockAt(x + 4, y + 1, z - 30).setType(mat);
		w.getBlockAt(x + 5, y + 1, z - 30).setType(mat);
		w.getBlockAt(x + 6, y + 1, z - 30).setType(mat);
		w.getBlockAt(x + 6, y + 1, z - 31).setType(mat);
		w.getBlockAt(x + 6, y + 1, z - 32).setType(mat);
		w.getBlockAt(x + 6, y + 1, z - 33).setType(mat);
		w.getBlockAt(x + 6, y + 1, z - 34).setType(mat);
		w.getBlockAt(x + 6, y + 1, z - 35).setType(mat);
		w.getBlockAt(x + 5, y + 1, z - 35).setType(mat);
		w.getBlockAt(x + 4, y + 1, z - 35).setType(mat);
		w.getBlockAt(x + 3, y + 1, z - 35).setType(mat);
		w.getBlockAt(x + 2, y + 1, z - 35).setType(mat);
		w.getBlockAt(x + 1, y + 1, z - 35).setType(mat);
		w.getBlockAt(x + 1, y + 1, z - 34).setType(mat);
		w.getBlockAt(x + 1, y + 1, z - 33).setType(mat);
		w.getBlockAt(x + 1, y + 1, z - 32).setType(mat);
		w.getBlockAt(x + 1, y + 1, z - 31).setType(mat);
		// the glass panes
		Material glassPane = Material.THIN_GLASS;
		w.getBlockAt(x + 1, y + 2, z - 30).setType(glassPane);
		w.getBlockAt(x + 2, y + 2, z - 30).setType(glassPane);
		w.getBlockAt(x + 3, y + 2, z - 30).setType(glassPane);
		w.getBlockAt(x + 4, y + 2, z - 30).setType(glassPane);
		w.getBlockAt(x + 5, y + 2, z - 30).setType(glassPane);
		w.getBlockAt(x + 6, y + 2, z - 30).setType(glassPane);
		w.getBlockAt(x + 6, y + 2, z - 31).setType(glassPane);
		w.getBlockAt(x + 6, y + 2, z - 32).setType(glassPane);
		w.getBlockAt(x + 6, y + 2, z - 33).setType(glassPane);
		w.getBlockAt(x + 6, y + 2, z - 34).setType(glassPane);
		w.getBlockAt(x + 6, y + 2, z - 35).setType(glassPane);
		w.getBlockAt(x + 5, y + 2, z - 35).setType(glassPane);
		w.getBlockAt(x + 4, y + 2, z - 35).setType(glassPane);
		w.getBlockAt(x + 3, y + 2, z - 35).setType(glassPane);
		w.getBlockAt(x + 2, y + 2, z - 35).setType(glassPane);
		w.getBlockAt(x + 1, y + 2, z - 35).setType(glassPane);
		w.getBlockAt(x + 1, y + 2, z - 34).setType(glassPane);
		w.getBlockAt(x + 1, y + 2, z - 33).setType(glassPane);
		w.getBlockAt(x + 1, y + 2, z - 32).setType(glassPane);
		w.getBlockAt(x + 1, y + 2, z - 31).setType(glassPane);

		if (victories >= 10) {
			if (victories <= 99) {
				buildnumber(victories);
			} else {
				buildnumber(99);
			}
		}
	}
	
	public void buildnumber(int number) {
		int numberRight = number % 10;
		int numberLeft = (number - numberRight) / 10;

		Material air = Material.AIR;

		for (int i = 0; i <= 4; i++) {
			for (int j = 0; j <= 6; j++) {
				if (NUMBERS[numberRight][j][i] == 1)
					w.getBlockAt(x + 2 - i, y + 27 - j, z).setType(MATS[4]);
				else
					w.getBlockAt(x + 2 - i, y + 27 - j, z).setType(air);
				if (NUMBERS[numberLeft][j][i] == 1)
					w.getBlockAt(x + 10 - i, y + 27 - j, z).setType(MATS[4]);
				else
					w.getBlockAt(x + 10 - i, y + 27 - j, z).setType(air);
			}
		}
	}
	
	public Location getPlatformLocation() {
		Location loc = w.getSpawnLocation();
		loc.setX(x + 1);
		loc.setY(y + 1);
		loc.setZ(z - 30);
		return loc;
	}

	public Location getSpectatorLocation() {
		Location loc = w.getSpawnLocation();
		loc.setX(x + 4);
		loc.setY(y + 1);
		loc.setZ(z - 32);
		return loc;
	}

	public Location getChampionLocation() {
		Location loc = w.getSpawnLocation();
		loc.setX(x + 4);
		loc.setY(y + 50);
		loc.setZ(z + 4);
		loc.setPitch(90);
		return loc;
	}
	
	public Location getLightningLocation() {
		Location loc = w.getSpawnLocation();
		loc.setX(x + 4);
		loc.setY(y);
		loc.setZ(z + 4);
		return loc;
	}
}
