package com.championsofthemap.cotm;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.entity.Player;

public class Team {
	private int victories;
	private int oldvictories;
	private String suffix;
	private ChatColor color;
	private String name;
	private HashMap<String, CotmPlayer> players = new HashMap<String, CotmPlayer>();

	public Team(int victories, String suffix, ChatColor color, String name, Cotm c) {
		this.victories = victories;
		this.oldvictories = victories;
		this.suffix = suffix;
		this.color = color;
		this.name = name;
	}

	public void resetValues(int victories, String suffix, ChatColor color, String name) {
		this.victories = victories;
		this.oldvictories = victories;
		this.suffix = suffix;
		this.color = color;
		this.name = name;
	}

	public boolean isChallenger() {
		return oldvictories == 0;
	}

	public int getVictories() {
		return victories;
	}
	
	public int getOldVictories() {
		return oldvictories;
	}
	
	/**
	 * Set the number of victories/points for this team
	 * 
	 * @param victories
	 */
	public void setVictories(int victories) {
		this.victories=victories;
		this.oldvictories=victories;
	}

	public void addLoss() {
		if (oldvictories > 0)
			victories = oldvictories - 1;
	}

	public void addVictorie() {
		victories = oldvictories + 1;
	}

	public String getSuffix() {
		return suffix;
	}

	public String getColorName() {
		return color.name();
	}

	public ChatColor getColor() {
		return color;
	}
	
	/**
	 * Set the team's color
	 * 
	 * @param color
	 */
	public void setColor(ChatColor color, Server s) {
		this.color = color;
		Player p;
		for(CotmPlayer cp : players.values()){
			p = s.getPlayerExact(cp.getName());
			if(p!=null)
				p.setDisplayName(p.getName() + " " + color + suffix);
		}
	}

	public String getTeamName() {
		return name;
	}
	
	public String getColoredTeamName() {
		return color + name;
	}
	
	/**
	 * Set the team's team name
	 * 
	 * @param name
	 */
	public void setTeamName(String name) {
		this.name = name;
	}

	public void addPlayer(CotmPlayer cp) {
		players.put(cp.getName(), cp);
	}

	public void removePlayer(String name) {
		players.remove(name);
	}

	public Collection<CotmPlayer> getTeamMembers() {
		return players.values();
	}

	public ArrayList<Player> getTeamPlayers() {
		ArrayList<Player> returnPlayers = new ArrayList<Player>();
		for (CotmPlayer cp : players.values()) {
			returnPlayers.add(cp.getPlayer());
		}
		return returnPlayers;
	}

	public String getTeamMembersByName() {
		String s = "";
		for (String name : players.keySet()) {
			s += name + ", ";
		}
		if (s.length() >= 2)
			return s.substring(0, s.length() - 2);
		else
			return s;
	}

	public boolean isTeamMember(Player p) {
		return players.containsKey(p.getName());
	}
	public boolean isTeamMember(String name) {
		return players.containsKey(name);
	}

	public int numberOfPlayers() {
		return players.size();
	}
	
	public String toString() {
		return this.color + this.name + "(" + this.suffix + ")";
	}
	
	/**
	 * Return a string containing the team info and players for this team
	 * 
	 * @return the team info
	 */
	public String getInfo() {
		String teamInfo = this.toString() + Cotm.MAIN_COLOR;

		if (this.getVictories() >= 0) {
			teamInfo += " has " + this.getVictories() + " victories/points";
		}

		if (this.numberOfPlayers() > 0)
			teamInfo += "\n" + "Team members: " + this.getTeamMembersByName();
		
		return teamInfo;
	}

}
