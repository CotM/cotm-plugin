package com.championsofthemap.cotm;

public enum PlayerType {

	PLAYER(false), REF(true), STREAMER(true), ASSISTANT(true);

	private boolean staff;

	PlayerType(boolean staff) {
		this.staff = staff;
	}
	
	public boolean isStaff(){
		return staff;
	}

}
