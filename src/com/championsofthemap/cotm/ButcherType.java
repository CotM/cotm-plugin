package com.championsofthemap.cotm;

public enum ButcherType {
	BUTCHER_HOSTILE("hostile"), BUTCHER_FRIENDLY("friendly"), BUTCHER_ALL("all");

	private final String butcherTypeName;

	ButcherType(String name) {
		this.butcherTypeName = name;
	}
	
	String getName(){
		return this.butcherTypeName;
	}

	/**
	 * @param arg The butcher argument sent
	 */
	public static ButcherType selectButcherType(String arg) {
		String a = arg.toLowerCase();
		if ("all".startsWith(a))
			return ButcherType.BUTCHER_ALL;
		else if ("hostile".startsWith(a))
			return ButcherType.BUTCHER_HOSTILE;
		else if ("friendly".startsWith(a))
			return ButcherType.BUTCHER_FRIENDLY;
		
		return null;
	}
}
