package com.championsofthemap.cotm;

public enum GameType {
	QUEUE_FIGHT("queue fight"), CHAMPIONSHIP_MATCH("championship match"), MEET_THE_MAP("Meet the Map"), SCRIMMAGE("scrimmage");

	private final String gameTypeName;

	GameType(String name) {
		this.gameTypeName = name;
	}
	
	String getName(){
		return this.gameTypeName;
	}
}
