/**
 * 
 */
package com.championsofthemap.cotm.loserdeath;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Silverfish;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.championsofthemap.cotm.Cotm;

/**
 * @author Caske33
 *
 */
public class SilverfishDeath extends LoserDeath{

	/**
	 * Constructs a SilverfishDeath.
	 * 
	 * @param platformLocation the location of the platform
	 */
	public SilverfishDeath(Location platformLocation) {
		super(platformLocation);
	}

	/**
	 * Makes the platform silverfish blocks and spawns a silverfish.
	 * 
	 * @see com.championsofthemap.cotm.loserdeath.LoserDeath#killLosers()
	 */
	@Override
	public void killLosers() {
		int x = platformLocation.getBlockX() - 1;
		int y = platformLocation.getBlockY() - 1;
		int z = platformLocation.getBlockZ() + 30;
		Material mat = Material.getMaterial(97); //the silverfish block
		
		for (int i = 2; i <= 5; i++) {
			for (int j = 31; j <= 34; j++) {
				world.getBlockAt(x + i, y, z - j).setType(mat);
			}
		}
		world.getBlockAt(x + 1, y + 1, z - 30).setType(mat);
		world.getBlockAt(x + 2, y + 1, z - 30).setType(mat);
		world.getBlockAt(x + 3, y + 1, z - 30).setType(mat);
		world.getBlockAt(x + 4, y + 1, z - 30).setType(mat);
		world.getBlockAt(x + 5, y + 1, z - 30).setType(mat);
		world.getBlockAt(x + 6, y + 1, z - 30).setType(mat);
		world.getBlockAt(x + 6, y + 1, z - 31).setType(mat);
		world.getBlockAt(x + 6, y + 1, z - 32).setType(mat);
		world.getBlockAt(x + 6, y + 1, z - 33).setType(mat);
		world.getBlockAt(x + 6, y + 1, z - 34).setType(mat);
		world.getBlockAt(x + 6, y + 1, z - 35).setType(mat);
		world.getBlockAt(x + 5, y + 1, z - 35).setType(mat);
		world.getBlockAt(x + 4, y + 1, z - 35).setType(mat);
		world.getBlockAt(x + 3, y + 1, z - 35).setType(mat);
		world.getBlockAt(x + 2, y + 1, z - 35).setType(mat);
		world.getBlockAt(x + 1, y + 1, z - 35).setType(mat);
		world.getBlockAt(x + 1, y + 1, z - 34).setType(mat);
		world.getBlockAt(x + 1, y + 1, z - 33).setType(mat);
		world.getBlockAt(x + 1, y + 1, z - 32).setType(mat);
		world.getBlockAt(x + 1, y + 1, z - 31).setType(mat);
		
		
		x = platformLocation.getBlockX() + 3;
		y = platformLocation.getBlockX() + 100;
		z = platformLocation.getBlockZ() - 3;
		if(y > world.getMaxHeight())
			y = world.getMaxHeight();
		Location loc = new Location(world, x, y, z);
		
		Silverfish sf;
		
		for(int i = 0; i<5; i++){
			sf = (Silverfish) world.spawnEntity(loc, EntityType.SILVERFISH);
			sf.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 1000000, 0));
			sf.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 1000000, 4));
		}
		
		
	}

	/**
	 * @see com.championsofthemap.cotm.loserdeath.LoserDeath#getMessage()
	 */
	@Override
	public String getMessage() {
		return Cotm.OK_COLOR + "It seems the losers are on a loose platform.";
	}

}
