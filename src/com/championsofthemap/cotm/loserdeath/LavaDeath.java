/**
 * 
 */
package com.championsofthemap.cotm.loserdeath;

import org.bukkit.Location;
import org.bukkit.Material;

import com.championsofthemap.cotm.Cotm;

/**
 * @author Caske33
 *
 */
public class LavaDeath extends LoserDeath{

	/**
	 * Creates a LavaDeath for the losers
	 * 
	 * @param platformLocation
	 */
	public LavaDeath(Location platformLocation) {
		super(platformLocation);
	}

	/** 
	 * This method places some lava above the heads of the losers
	 * 
	 * @see com.championsofthemap.cotm.loserdeath.LoserDeath#killLosers()
	 */
	@Override
	public void killLosers() {
		Material mat = Material.LAVA;
		
		int x = platformLocation.getBlockX();
		int y = platformLocation.getBlockY();
		int z = platformLocation.getBlockZ();
		
		for (int i = 0; i <= 5; i++) {
			for (int j = 0; j <= 5; j++) {
				world.getBlockAt(x + i, y + 15, z - j).setType(mat);
			}
		}
	}

	/**
	 * @see com.championsofthemap.cotm.loserdeath.LoserDeath#getMessage()
	 */
	@Override
	public String getMessage() {
		return Cotm.OK_COLOR + "A bright light has appeard above the losers.";
	}
}
