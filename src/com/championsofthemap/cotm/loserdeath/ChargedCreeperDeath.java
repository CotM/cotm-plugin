/**
 * 
 */
package com.championsofthemap.cotm.loserdeath;

import org.bukkit.Location;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.EntityType;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.championsofthemap.cotm.Cotm;

/**
 * @author Caske33
 *
 */
public class ChargedCreeperDeath extends LoserDeath{

	/**
	 * Creates a ChargedCreeperDeath for the losers
	 * 
	 * @param platformLocation
	 */
	public ChargedCreeperDeath(Location platformLocation) {
		super(platformLocation);
	}

	/**
	 * This methods spawns charged creepers above the losers.
	 * 
	 * @see com.championsofthemap.cotm.loserdeath.LoserDeath#killLosers()
	 */
	@Override
	public void killLosers() {
		
		int x = platformLocation.getBlockX() + 3;
		int y = platformLocation.getBlockX() + 100;
		int z = platformLocation.getBlockZ() - 3;
		if(y > world.getMaxHeight())
			y = world.getMaxHeight();
		
		Location loc = new Location(world, x, y, z);
		
		Creeper c = (Creeper) world.spawnEntity(loc, EntityType.CREEPER);
		c.setPowered(true);
		c.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 10000000, 4));
		
	}

	/**
	 * @see com.championsofthemap.cotm.loserdeath.LoserDeath#getMessage()
	 */
	@Override
	public String getMessage() {
		return Cotm.OK_COLOR + "They did lose the match but it seems they are exploding of ideas.";
	}

}
