package com.championsofthemap.cotm.loserdeath;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.World;

/**
 * This method is the base method for the multiple deaths possible for the losers after the match.
 * 
 * @author Caske33
 * 
 */
public abstract class LoserDeath {
	/**
	 * This is the location of the platform where the losers are.
	 */
	protected Location platformLocation;
	/**
	 * This is the world in which the platform exists.
	 */
	protected World world;
	
	/**
	 * Construct a loserDeath
	 * 
	 * @param platformLocation this is the location of the platform (of the losers)
	 */
	public LoserDeath(Location platformLocation){
		this.platformLocation = platformLocation;
		this.world = platformLocation.getWorld();
	}
	
	/**
	 * This methods places all the blocks that will kill the losers.
	 */
	public abstract void killLosers();
	
	/**
	 * This method returns a random death.
	 * 
	 * @param platformLocation this is the location of the platform (of the losers)
	 * @return the random deathobject
	 */
	public static LoserDeath getRandomDeath(Location platformLocation){
		Random r = new Random();
		
		//When adding more deaths, don't forget to change number on the next line too!
		int index = r.nextInt(3);
		//For testing: uncomment next line and put the correct index
		//index = 2;
		if(index == 0)
			return new LavaDeath(platformLocation);
		if(index == 1)
			return new ChargedCreeperDeath(platformLocation);
		if(index == 2)
			return new SilverfishDeath(platformLocation);
		
		return null;
				
	}
	
	/**
	 * Gets the message that will be sent to the ops on the server
	 * @return the message
	 */
	public abstract String getMessage();

}
