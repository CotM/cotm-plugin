package com.championsofthemap.cotm;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * This class provides the different special mobs that can be spawned with
 * 'specialspawn'.
 * 
 * @author Caske33
 * 
 */
public enum SpecialMob {

	JUMP_SPIDER(EntityType.SPIDER, PotionEffectType.JUMP, "Jumpy_spider"), INVINCIBLE_PIG(EntityType.PIG, PotionEffectType.DAMAGE_RESISTANCE,
			"Invincible_pig"), IMFLAMMABLE_VILLAGER(EntityType.VILLAGER, PotionEffectType.FIRE_RESISTANCE, "Imflammable_villager"), POISONED_COW(
			EntityType.COW, PotionEffectType.POISON, "Poisoned_cow"), WEAK_BLAZE(EntityType.BLAZE, PotionEffectType.WEAKNESS, "Weakened_blaze");

	private EntityType entityType;
	private PotionEffectType potionType;
	private String name;

	SpecialMob(EntityType et, PotionEffectType pet, String name) {
		this.entityType = et;
		this.potionType = pet;
		this.name = name;
	}

	/**
	 * This methods spawns the special mob with message to the server.
	 * 
	 * @param location the location where to spawn the mob.
	 */
	public void spawn(Location location, Server s) {
		World world = location.getWorld();
		LivingEntity le = (LivingEntity) world.spawnEntity(location, entityType);
		le.addPotionEffect(new PotionEffect(potionType, 1000000000, 10));
		s.broadcastMessage(ChatColor.YELLOW + name + " joined the game.");
	}
}
